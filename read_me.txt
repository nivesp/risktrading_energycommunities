The document 00_ElectronicCompanion provides extended problem formulation
of the manuscipt "Risk Trading in Energy Communities".

The file run_me.py has to be executed to run models.

The file run_me_NumberScenarios.py assess the appropriate amount of scenarios
to be considered.