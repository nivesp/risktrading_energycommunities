''' 
=========================================================================
Script         : Model settings
Author         : Niklas Vepermann
Project        : Risk trading in energy communities
=========================================================================
'''
# Rn
####

Rn_Settings = {
'how' :			['Rn'],
#
'variables' : 	['powerPl_f',
				'powerArbBuy_f','powerArbSell_f',
#
				'powerPl_s',
				'powerArbBuy_s','powerArbSell_s'],
#
'objective' : 	['Rn'],
#
'constraints' : ['EQ_PowerPlMax_lb_f','EQ_PowerPlMax_ub_f'
				'EQ_PowerArbBuyMax_f','EQ_PowerArbSellMax_f',
#
				'EQ_balancePl_s',
				'EQ_PowerPlMax_lb_s','EQ_PowerPlMax_ub_s'
				'EQ_PowerArbBuyMax_s','EQ_PowerArbSellMax_s',
#
				'EQ_balanceLoc_f','EQ_balanceLoc_s'],
#
'results' : 	['objective',
#
				'powerPl_f',
				'powerArbBuy_f','powerArbSell_f',
				'dual_balanceLoc_f',
#
				'powerPl_s',
				'powerArbBuy_s','powerArbSell_s',
				'dual_balanceLoc_s'],
#				
'eval' :		['OperationPayoffs'] }


# Ra
####

Ra_sp_Settings = {
'how' :			['Ra_sp'],
#
'variables' : 	['powerPl_f',
				'powerArbBuy_f','powerArbSell_f',
#
				'powerPl_s',
				'powerArbBuy_s','powerArbSell_s',
#				
				'eta','var'],
#
'objective' : 	['Ra_sp'],
#
'constraints' : ['EQ_PowerPlMax_lb_f','EQ_PowerPlMax_ub_f'
				'EQ_PowerArbBuyMax_f','EQ_PowerArbSellMax_f',
#
				'EQ_balancePl_s',
				'EQ_PowerPlMax_lb_s','EQ_PowerPlMax_ub_s'
				'EQ_PowerArbBuyMax_s','EQ_PowerArbSellMax_s',
#
				'EQ_balanceLoc_f','EQ_balanceLoc_s',
#
				'EQ_Ra_CVaR'],
#
'results' : 	['objective',
#
				'powerPl_f',
				'powerArbBuy_f','powerArbSell_f',
				'dual_balanceLoc_f',
#
				'powerPl_s',
				'powerArbBuy_s','powerArbSell_s',
				'dual_balanceLoc_s',
#
				'eta','var','CVaR',
				'dual_Ra_CVaR'],
#				
'eval' :		['OperationPayoffs'] }	


Ra_ADVolume_Settings = {
'how' :			['Ra_ADVolume'],
#
'variables' : 	['eta_n','var_n','a_n',
				 'eta_b','var_b','b'],
#
'objective' : 	['Ra_ADVolume'],
#
'constraints' : ['EQ_Ra_CVaR_ad_Player',
				 'EQ_Ra_CVaR_ad_Arb',
				 'EQ_Ra_Bal_ad'],
#
'results' : 	['objective',
#
				'eta_n','var_n','CVaR_n','dual_Ra_Bal_ad',
				'dual_Ra_CVaR_ad_Player','dual_Ra_CVaR_ad_Arb',
				'a_n','b'],
#				
'eval' :		[''] }
