''' 
=========================================================================
Script         : Input data
Author         : Niklas Vepermann
Project        : Risk trading in energy communities
=========================================================================
'''

# Sets
Players = ['Player1', 'Player2', 'Player3']
Number_of_TimeSteps = 1
Time = ['t%d' % x for x in range(1,Number_of_TimeSteps+1)]


### PV Scenarios ###
from func.PVScenarios import gen_PVScenarios
from func.PVScenarios import gen_PVInput
from func.PVScenarios import clustered_PV

# generate PV power scenarios
PV_mean = [0.7] # Expected PV power generation	

Number_Samples = 1000 # Sampling scenarios	
FullSample_of_PV = gen_PVScenarios(PV_mean,Number_Samples) # generate scenarios	


# Get PV Scenarios for opt

# choose number of scnearios
Number_of_Scenarios = 6
#Number_of_Scenarios = 500

#PV_scenarios = sorted(FullSample_of_PV[0:Number_of_Scenarios]) # sorted by values
PV_scenarios = clustered_PV(FullSample_of_PV,Number_of_Scenarios) # clustered PV

# for reference PV scenario please see function gen_PVInput and uncomment PV_player1
Weight,Scenarios,PowerPV,PV_player1 = gen_PVInput(Number_of_Scenarios,PV_scenarios,Players,Time)


# Else
Epsilon = 0.5
Reg = 0.01

# Risk aversion
#Alpha_CVaR_b = 1.0
Alpha_CVaR_b = 0.9
Alpha_CVaR_n = {'Player1' : 0.5,
			    'Player2' : 0.3,
			    'Player3' : 0.7
				}

Alpha_CVaR = Alpha_CVaR_b # confidence level; system cost higher than

PriceBuy = {'t1' : 0.50}          

PriceSell = { key : 0.5*value for key,value in PriceBuy.items()}

Demand_AUX = {'t1' : 10}
			  
# Demand 
Demand = {(player,key) : value for player in Players 
    for key,value in Demand_AUX.items()}

# Player1 demand set to zero
Demand.update({('Player1',key[1]) : round(1.0*value,2) 
	for key,value in Demand.items()})
	
# Player2 demand
Demand.update({('Player2',key[1]) : round(0*value,2) 
	for key,value in Demand.items()})

# Player3 demand set to zero
Demand.update({('Player3',key[1]) : round(1.0*value,2) 
	for key,value in Demand.items()})


PowerPlMax = {(player) : 50 for player in Players}
PowerArbBuyMax = 50
PowerArbSellMax = 50