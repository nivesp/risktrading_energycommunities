''' 
=========================================================================
Script         : Generates and solves opt problems
Author         : Niklas Vepermann
Project        : Risk trading in energy communities
=========================================================================
'''
import pandas as pd    # Pandas
import numpy as np
import time
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib2tikz

# for GAMS
from gams import *
import subprocess 
import gdxpds 

from collections import OrderedDict 

from OptProblem_RiskTrading import OptProblem

# load model settings
exec(open('./data/input/ModelSettings.py').read())
# load data
exec(open('./data/input/InputData_case5_50scenarios_AUX.py').read())


Rn_var_lb = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
Rn_var_ub = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
Rn_payoff = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])

List_of_Scenarios = []


for NumberScenarios in list(range(1,Number_of_Scenarios+1,1)):

	from func.PVScenarios import clustered_PV

	Number_of_Scenarios = NumberScenarios
	#PV_scenarios = sorted(FullSample_of_PV[0:Number_of_Scenarios]) # sorted by values
	PV_scenarios = clustered_PV(FullSample_of_PV,NumberScenarios) # clustered PV
	Weight,Scenarios,PowerPV,PV_player1 = gen_PVInput(Number_of_Scenarios,PV_scenarios,Players,Time)

	# Risk neutral
	##############
	
	model_name = 'Rn'

	Rn_Input = 	{'Players' : Players,
				 'Scenarios' : Scenarios,
				 'Time' : Time,
				 #
				 'PriceBuy' : PriceBuy,
				 'PriceSell' : PriceSell,
				 #
				 'Weight' : Weight,
				 'PowerPV' : PowerPV,
				 #
				 'Demand' : Demand,
				 #
				 'Epsilon' : Epsilon,
				 'Reg' : Reg,
				 #
				 'PowerPlMax' : PowerPlMax,
				 #
				 'PowerArbBuyMax' : PowerArbBuyMax,
				 'PowerArbSellMax' : PowerArbSellMax}  

	# Initialize model and run Optimization
	Rn = OptProblem(model_name,Rn_Input,Rn_Settings) 
	Rn.run_opt()
	Rn.get_results(Rn_Settings)

	# Collect results
	#################
	Rn_var_lb = Rn_var_lb.append(pd.DataFrame(
		Rn.Payoff_per_scenario.mean() - Rn.Payoff_per_scenario.std()).transpose(), 
		ignore_index=True)
	Rn_var_ub = Rn_var_ub.append(pd.DataFrame(
		Rn.Payoff_per_scenario.mean() + Rn.Payoff_per_scenario.std()).transpose(), 
		ignore_index=True)
	Rn_payoff = Rn_payoff.append(Rn.Payoff_expected, ignore_index=True)
	
	# Wasserstein distance
	flat_list = []
	for sublist in PV_scenarios:
		for item in sublist:
			flat_list.append(item)

	List_of_Scenarios.append(flat_list)


rangeStudy = list(range(1,Number_of_Scenarios+1,1))	
	
from scipy import stats
WDistance = ['NaN']
for x in rangeStudy[:-1]:
	WDistance.append(round(stats.wasserstein_distance(List_of_Scenarios[x-1],List_of_Scenarios[x]),3))
	
	
# Plot results	
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
#
ax1.plot(rangeStudy,Rn_payoff['Tot'],color='black',label='System cost')
ax1.fill_between(rangeStudy,Rn_var_lb['Tot'], Rn_var_ub['Tot'],
	color='gray',alpha=0.5,label='standard deviation')
ax1.plot(rangeStudy[1],WDistance[1], color='red',label='Wasserstein distance')
ax1.set_ylabel('System cost')
ax1.legend()
ax1.set_xlim(0,max(rangeStudy))
#
ax2 = ax1.twinx()
ax2.plot(rangeStudy,WDistance, color='red')
ax2.set_ylabel('Wasserstein distance')
matplotlib2tikz.save('./data/output/graphics/%s_Opt_Number_Scenarios_scen%s.pdf' 
	% (datetime.today().strftime('%Y%m%d'),str(Number_of_Scenarios)),
		   figureheight = '\\fheight',
		   figurewidth = '\\fwidth')
plt.savefig('./data/output/graphics/%s_Opt_Number_Scenarios_scen%s.pdf' 
	% (datetime.today().strftime('%Y%m%d'),str(Number_of_Scenarios)),
	format='pdf', dpi=1000)
