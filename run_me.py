''' 
=========================================================================
Script         : Generates and solves opt problems
Author         : Niklas Vepermann
Project        : Risk trading in energy communities
=========================================================================
'''
# load packages
import pandas as pd
import numpy as np
import time
from datetime import datetime

# for GAMS
from gams import *
import subprocess 
import gdxpds 

from collections import OrderedDict 


# load local optimization problems:
# -> contains optimization problems and post processing of results
from OptProblem_RiskTrading import OptProblem

# load model settings
# -> provides input to optimization problems with respect to
# - parameters
# - variables
# - constraints
# - post processing
exec(open('./data/input/ModelSettings.py').read())

# load data: 
# -> choose appropriate InputData file by setting the path
exec(open('./data/input/InputData.py').read())


# Parameters, which collect results in the following for loop:
# expected payoff, lower bound and upper bound of variance arising from
# scenarios
#
# risk-neutral case
Rn_var_lb = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
Rn_var_ub = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
Rn_payoff = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
#
# risk-averse case with complete risk market
Ra_ADVolume_var_lb = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
Ra_ADVolume_var_ub = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
Ra_ADVolume_payoff = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
#
# risk-averse case with partially incomplete risk market
Ra_i_var_lb = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
Ra_i_var_ub = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
Ra_i_payoff = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
#
# risk-averse case with fully incomplete risk market
Ra_fi_var_lb = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
Ra_fi_var_ub = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])
Ra_fi_payoff = pd.DataFrame(columns=['Arb','Player1','Player2','Player3','Tot'])


# Activate to study increasing risk-aversion of prosumer 1 from
# from alpha = 1 to alpha = 0.05:
#rangeStudy = list(reversed([x/100 for x in range(5,100+5,5) ]))

# Set rangeStudy according to risk aversion of prosumer 1
rangeStudy = [0.5]

start = time.time() # times the duration of iteration
for CVaR in rangeStudy:

	# updates risk aversion of prosumer 1
	Alpha_CVaR_n.update({'Player1' : CVaR})

	# Risk neutral
	##############
	start_rn = time.time()
	
	model_name = 'Rn'

	Rn_Input = 	{'Players' : Players,
				 'Scenarios' : Scenarios,
				 'Time' : Time,
				 #
				 'PriceBuy' : PriceBuy,
				 'PriceSell' : PriceSell,
				 #
				 'Weight' : Weight,
				 'PowerPV' : PowerPV,
				 #
				 'Demand' : Demand,
				 #
				 'Epsilon' : Epsilon,
				 'Reg' : Reg,
				 #
				 'PowerPlMax' : PowerPlMax,
				 #
				 'PowerArbBuyMax' : PowerArbBuyMax,
				 'PowerArbSellMax' : PowerArbSellMax}  

	# Initialize model and run Optimization
	Rn = OptProblem(model_name,Rn_Input,Rn_Settings) 
	Rn.run_opt()
	Rn.get_results(Rn_Settings)

	# Collect results
	#################
	Rn_var_lb = Rn_var_lb.append(pd.DataFrame(
		Rn.Payoff_per_scenario.mean() - Rn.Payoff_per_scenario.std()).transpose(), 
		ignore_index=True)
	Rn_var_ub = Rn_var_ub.append(pd.DataFrame(
		Rn.Payoff_per_scenario.mean() + Rn.Payoff_per_scenario.std()).transpose(), 
		ignore_index=True)
	Rn_payoff = Rn_payoff.append(Rn.Payoff_expected, ignore_index=True)

	stop_rn = time.time()
	CompTime_rn = round(stop_rn - start_rn,2)


	# Risk averse complete
	######################
	start_ra_c = time.time()

	# Ra sp
	model_name = 'Ra_sp'

	Ra_sp_Input = 	{'Players' : Players,
					 'Scenarios' : Scenarios,
					 'Time' : Time,
					 #
					 'PriceBuy' : PriceBuy,
					 'PriceSell' : PriceSell,
					 #
					 'Weight' : Weight,
					 'PowerPV' : PowerPV,
					 #
					 'Demand' : Demand,
					 #
					 'Epsilon' : Epsilon,
					 'Reg' : Reg,
					 #
					 'PowerPlMax' : PowerPlMax,
					 #
					 'PowerArbBuyMax' : PowerArbBuyMax,
					 'PowerArbSellMax' : PowerArbSellMax,
					 #
					 'Alpha_CVaR' : Alpha_CVaR
					 }  

	# Initialize model and run Optimization
	Ra_sp = OptProblem(model_name,Ra_sp_Input,Ra_sp_Settings) 
	Ra_sp.run_opt()
	Ra_sp.get_results(Ra_sp_Settings)


	# AD Volume
	model_name = 'Ra_ADVolume'

	# Aribtrager
	Opt_powerArbBuy_f = Ra_sp.powerArbBuy_f
	Opt_powerArbSell_f = Ra_sp.powerArbSell_f

	Opt_powerArbBuy_s = Ra_sp.powerArbBuy_s
	Opt_powerArbSell_s = Ra_sp.powerArbSell_s

	# Player
	Opt_powerPl_f = Ra_sp.powerPl_f
	Opt_powerPl_s = Ra_sp.powerPl_s

	# Prices
	#Opt_dual_balanceLoc_f = Ra_sp.dual_balanceLoc_f
	#Opt_dual_balanceLoc_s = Rn.dual_balanceLoc_s
	Opt_dual_balanceLoc_f = Ra_sp.dual_balanceLoc_f
	Opt_dual_balanceLoc_s = {key : Alpha_CVaR*value for key,value in Ra_sp.dual_balanceLoc_s.items()}
	
	Opt_pi = {key : -1*value for key,value in Ra_sp.dual_Ra_CVaR.items()}

	Ra_ADVolume_Input = {'Players' : Players,
						 'Scenarios' : Scenarios,
						 'Time' : Time,
						 #
						 'PriceBuy' : PriceBuy,
						 'PriceSell' : PriceSell,
						 #
						 'Weight' : Weight,
						 'PowerPV' : PowerPV,
						 #
						 'Demand' : Demand,
						 #
						 'Epsilon' : Epsilon,
						 'Reg' : Reg,
						 #
						 'Opt_powerArbBuy_f' : Opt_powerArbBuy_f,
						 'Opt_powerArbSell_f' : Opt_powerArbSell_f,
						 'Opt_powerArbBuy_s' : Opt_powerArbBuy_s,
						 'Opt_powerArbSell_s' : Opt_powerArbSell_s,
						 #
						 'Opt_powerPl_f' : Opt_powerPl_f,
						 'Opt_powerPl_s' : Opt_powerPl_s,
						 #
						 'Opt_dual_balanceLoc_f' : Opt_dual_balanceLoc_f,
						 'Opt_dual_balanceLoc_s' : Opt_dual_balanceLoc_s,
						 'Opt_pi' : Opt_pi,
						 #
						 'Alpha_CVaR_n' : Alpha_CVaR_n,
						 'Alpha_CVaR_b' : Alpha_CVaR_b
						 }  

	# Initialize model and run Optimization
	Ra_ADVolume = OptProblem(model_name,Ra_ADVolume_Input,Ra_ADVolume_Settings) 
	Ra_ADVolume.run_opt()
	Ra_ADVolume.get_results(Ra_ADVolume_Settings)

	# Collect results
	Ra_ADVolume_var_lb = Ra_ADVolume_var_lb.append(pd.DataFrame(
		Ra_ADVolume.Payoff_per_scenario.mean() - Ra_ADVolume.Payoff_per_scenario.std()).transpose(), 
		ignore_index=True)
	Ra_ADVolume_var_ub = Ra_ADVolume_var_ub.append(pd.DataFrame(
		Ra_ADVolume.Payoff_per_scenario.mean() + Ra_ADVolume.Payoff_per_scenario.std()).transpose(), 
		ignore_index=True)
	Ra_ADVolume_payoff = Ra_ADVolume_payoff.append(Ra_ADVolume.Payoff_expected, ignore_index=True)

	stop_ra_c = time.time()
	CompTime_ra_c = round(stop_ra_c - start_ra_c,2)

	# Risk averse incomplete
	########################
	start_ra_pi = time.time()
	
	model_name = 'Ra_i'

	Ra_i = OptProblem(model_name,Ra_ADVolume_Input,Ra_ADVolume_Settings)

	Z = {key : 0 for key in Scenarios}
	for w in Scenarios[1:int(round(len(Scenarios)/3,0))+1]: # choose devider for fraction of available Arrow-Debreu securities
		Z.update({w : 1e12}) # upper and lower bound for Arrow-Debreu securities

	Ra_i_Input = {'Players' : Players,
				 'Scenarios' : Scenarios,
				 'Time' : Time,
				 #
				 'PriceBuy' : PriceBuy,
				 'PriceSell' : PriceSell,
				 #
				 'Weight' : Weight,
				 'PowerPV' : PowerPV,
				 #
				 'Demand' : Demand,
				 #
				 'Epsilon' : Epsilon,
				 'Reg' : Reg,
				 #
				 'PowerPlMax' : PowerPlMax,
				 #
				 'PowerArbBuyMax' : PowerArbBuyMax,
				 'PowerArbSellMax' : PowerArbSellMax,
				 #
				 'Alpha_CVaR_n' : Alpha_CVaR_n,
				 'Alpha_CVaR_b' : Alpha_CVaR_b,
				 #
				 'Z' : Z
				 }   

	# generate gdx file that is suitable for GAMS
	from func.func import data_to_Gams
	data_to_Gams(Ra_i_Input)

	# run GAMS
	subprocess.call([r"C:\GAMS\win64\24.6\gams.exe", 
		r".\Ra_fi.gms"])

	# read result from GAMS
	gdx_file = r".\data\gams_to_data.gdx"
	Ra_i.results_GAMS = gdxpds.to_dataframes(gdx_file)

	Ra_i.powerPl_f = {}
	nn = 0
	for n in Players:
		Ra_i.powerPl_f.update({n : Ra_i.results_GAMS['powerPl_f']['Level'].tolist()[nn:nn+1][0]})
		nn = nn+1 

	# PAYOFFS TO DATAFRAME #
	from func.func import gams_payoff_to_df
	Ra_i.Payoff_per_scenario,Ra_i.Payoff_expected,Ra_i.PaymentFlow_expected = (
		gams_payoff_to_df(Ra_i.results_GAMS,Scenarios,Players))	
		
	# Collect results
	#################
	Ra_i_var_lb = Ra_i_var_lb.append(pd.DataFrame(
		Ra_i.Payoff_per_scenario.mean() - Ra_i.Payoff_per_scenario.std()).transpose(), 
		ignore_index=True)
	Ra_i_var_ub = Ra_i_var_ub.append(pd.DataFrame(
		Ra_i.Payoff_per_scenario.mean() + Ra_i.Payoff_per_scenario.std()).transpose(), 
		ignore_index=True)
	Ra_i_payoff = Ra_i_payoff.append(Ra_i.Payoff_expected, ignore_index=True)

	stop_ra_pi = time.time()
	CompTime_ra_pi = round(stop_ra_pi - start_ra_pi,2)
	
	
	# Risk averse fully incomplete
	##############################
	start_ra_fi = time.time()
	
	model_name = 'Ra_fi'

	Ra_fi = OptProblem(model_name,Ra_ADVolume_Input,Ra_ADVolume_Settings)

	Z = {key : 0 for key in Scenarios} # lower and upper bound for Arrow-Debreu securities is zero
	
	#Z = {key : 0 for key in Scenarios}
	#for w in Scenarios[1:int(round(len(Scenarios)/3,0))+1]: # choose devider for fraction of available Arrow-Debreu securities
	#	Z.update({w : 1e12}) # upper and lower bound for Arrow-Debreu securities
		
	Ra_fi_Input = {'Players' : Players,
				 'Scenarios' : Scenarios,
				 'Time' : Time,
				 #
				 'PriceBuy' : PriceBuy,
				 'PriceSell' : PriceSell,
				 #
				 'Weight' : Weight,
				 'PowerPV' : PowerPV,
				 #
				 'Demand' : Demand,
				 #
				 'Epsilon' : Epsilon,
				 'Reg' : Reg,
				 #
				 'PowerPlMax' : PowerPlMax,
				 #
				 'PowerArbBuyMax' : PowerArbBuyMax,
				 'PowerArbSellMax' : PowerArbSellMax,
				 #
				 'Alpha_CVaR_n' : Alpha_CVaR_n,
				 'Alpha_CVaR_b' : Alpha_CVaR_b,
				 #
				 'Z' : Z
				 }   

	# generate gdx file that is suitable for GAMS
	from func.func import data_to_Gams
	data_to_Gams(Ra_fi_Input)

	# run GAMS
	subprocess.call([r"C:\GAMS\win64\24.6\gams.exe", 
		r".\Ra_fi.gms"])

	# get results from GAMS
	gdx_file = r".\data\gams_to_data.gdx"
	Ra_fi.results_GAMS = gdxpds.to_dataframes(gdx_file)

	Ra_fi.powerPl_f = {}
	nn = 0
	for n in Players:
		Ra_fi.powerPl_f.update({n : Ra_fi.results_GAMS['powerPl_f']['Level'].tolist()[nn:nn+1][0]})
		nn = nn+1 

	# PAYOFFS TO DATAFRAME #
	from func.func import gams_payoff_to_df
	Ra_fi.Payoff_per_scenario,Ra_fi.Payoff_expected,Ra_fi.PaymentFlow_expected = (
		gams_payoff_to_df(Ra_fi.results_GAMS,Scenarios,Players))	

	# Collect results
	#################
	Ra_fi_var_lb = Ra_fi_var_lb.append(pd.DataFrame(
		Ra_fi.Payoff_per_scenario.mean() - Ra_fi.Payoff_per_scenario.std()).transpose(), 
		ignore_index=True)
	Ra_fi_var_ub = Ra_fi_var_ub.append(pd.DataFrame(
		Ra_fi.Payoff_per_scenario.mean() + Ra_fi.Payoff_per_scenario.std()).transpose(), 
		ignore_index=True)
	Ra_fi_payoff = Ra_fi_payoff.append(Ra_fi.Payoff_expected, ignore_index=True)

	stop_ra_fi = time.time()
	CompTime_ra_fi = round(stop_ra_fi - start_ra_fi,2)

	print('\n Alpha CVaR of prosumer 1 is', str(CVaR) ) # show iteration


# Timing execution
end = time.time()
print('\n Solving Time: %d min \n' % (round((end - start)/60,2)) )



# EVALUATION #


#################################################
# Bar plot of AD trades in a complete risk market
#################################################
from func.func import plot_bar_ADTrades

# Activate to plot Arrow-Debreu security trades per scenarios:
#plot_bar_ADTrades(Ra_ADVolume)


##############################################################
# Payoff distribution of prosumers and the spatial arbitrageur
##############################################################
from func.func import payoff_dist

# per scenario #
################

# initialize DataFrame
payoff_distribution = pd.DataFrame({('Player1','Ra_fi') : 
	np.zeros(len(Rn.Payoff_per_scenario)).tolist()},
	index=Rn.Payoff_per_scenario.index.tolist())

# add Prosumers
for n in Players:
	payoff_distribution[n,'Ra_fi'] = Ra_fi.Payoff_per_scenario[n]
	payoff_distribution[n,'Ra_i'] = Ra_i.Payoff_per_scenario[n]
	payoff_distribution[n,'Ra_ADVolume'] = Ra_ADVolume.Payoff_per_scenario[n]
	payoff_distribution[(n,'Rn')] = Rn.Payoff_per_scenario[n]
# add Arbitrageur
payoff_distribution['Arb','Ra_fi'] = Ra_fi.Payoff_per_scenario['Arb']
payoff_distribution['Arb','Ra_i'] = Ra_i.Payoff_per_scenario['Arb']
payoff_distribution['Arb','Ra_ADVolume'] = Ra_ADVolume.Payoff_per_scenario['Arb']
payoff_distribution[('Arb','Rn')] = Rn.Payoff_per_scenario['Arb']

# expected #
############

# initialize DataFrame
payoff_expected = pd.DataFrame({('Player1','Ra_fi') : 0 }, index=[0] )

# add Prosumers
for n in Players:
	payoff_expected[n,'Ra_fi'] = Ra_fi.Payoff_expected[n].tolist()
	payoff_expected[n,'Ra_i'] = Ra_i.Payoff_expected[n].tolist()
	payoff_expected[n,'Ra_ADVolume'] = Ra_ADVolume.Payoff_expected[n].tolist()
	payoff_expected[(n,'Rn')] = Rn.Payoff_expected[n].tolist()
# add Arbitrageur
payoff_expected['Arb','Ra_fi'] = Ra_fi.Payoff_expected['Arb'].tolist()
payoff_expected['Arb','Ra_i'] = Ra_i.Payoff_expected['Arb'].tolist()
payoff_expected['Arb','Ra_ADVolume'] = Ra_ADVolume.Payoff_expected['Arb'].tolist()
payoff_expected[('Arb','Rn')] = Rn.Payoff_expected['Arb'].tolist()

# Activate to plot payoff distribution:	
payoff_dist(payoff_expected,payoff_distribution,Players)


##########################
# System cost distribution
##########################
from func.func import payoff_dist_tot

# per scenario #
################

# initialize DataFrame
System_Cost_per_scenario = pd.DataFrame({}, index=Ra_fi.Scenarios)

System_Cost_per_scenario['Ra_fi'] = ( 
	Ra_fi.results_GAMS['system_cost_per_scenario']['Value'].tolist() )
System_Cost_per_scenario['Ra_i'] = ( 
	Ra_i.results_GAMS['system_cost_per_scenario']['Value'].tolist() )
System_Cost_per_scenario['Ra_ADVolume'] = ( 
	Ra_sp.System_Cost_per_scenario.values() )	
System_Cost_per_scenario['Rn'] = ( 
	Rn.System_Cost_per_scenario.values() )
	
# expected #
############

# initialize DataFrame
System_Cost_expected = pd.DataFrame({}, index=['Player1','Player2','Player3','Arb'])
z_AUX = Ra_fi.results_GAMS['system_cost_expected_Player']['Value'].tolist()
z_AUX.append(Ra_fi.results_GAMS['system_cost_expected_Arb']['Value'].tolist()[0])
System_Cost_expected['Ra_fi'] = z_AUX
z_AUX = Ra_i.results_GAMS['system_cost_expected_Player']['Value'].tolist()
z_AUX.append(Ra_i.results_GAMS['system_cost_expected_Arb']['Value'].tolist()[0])
System_Cost_expected['Ra_i'] = z_AUX
System_Cost_expected['Ra_ADVolume'] = ( Ra_sp.System_Cost_expected )
System_Cost_expected['Rn'] = ( Rn.System_Cost_expected )

# Activate to plot system cost distribution:	
payoff_dist_tot(System_Cost_expected,System_Cost_per_scenario)


##################################################
# Impact of increasing risk aversion of prosumer 1
###################################################

from func.func import plot_Ra_fi_Costs
case = ['Rn','Ra_ADVolume','Ra_i','Ra_fi']
payoff = [Rn_payoff,Ra_ADVolume_payoff,Ra_i_payoff,Ra_fi_payoff]
var_lb = [Rn_var_lb,Ra_ADVolume_var_lb,Ra_i_var_lb,Ra_fi_var_lb]
var_ub = [Rn_var_ub,Ra_ADVolume_var_ub,Ra_i_var_ub,Ra_fi_var_ub]

case = ['Ra_ADVolume','Ra_fi']
payoff = [Ra_ADVolume_payoff,Ra_fi_payoff]
var_lb = [Ra_ADVolume_var_lb,Ra_fi_var_lb]
var_ub = [Ra_ADVolume_var_ub,Ra_fi_var_ub]

# Activate to plot impact of increasing risk aversion:
#for x in range(0,len(case)):
#	plot_Ra_fi_Costs(case[x],rangeStudy,payoff[x],var_lb[x],var_ub[x])


##########################
# Expected payment flows # 
##########################

from func.func import bar_plot
EvalSet = [Ra_ADVolume.PaymentFlow_expected,Ra_i.PaymentFlow_expected,Ra_fi.PaymentFlow_expected]
modelname = ['complete','partially incompelte','fully incompelte']

z = 0
for InputDF in EvalSet:
	bar_plot(InputDF,modelname[z])
	z = z + 1