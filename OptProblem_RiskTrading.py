''' 
=========================================================================
Script         : Opt problems: Rn, Ra-fc, Ra-fi
Author         : Niklas Vepermann
Project        : Risk trading in energy communities
=========================================================================
''' 

from gurobipy import * # Gurobi
import pandas as pd    # Pandas
from collections import OrderedDict 

class OptProblem():
    def __init__(self,Name,Input,Settings):
        self.m = Model(Name)
        self.create_sets_parameters(Input)
        self.create_variables(Settings)
        self.create_objective(Settings)
        self.create_constraints(Settings)


    def run_opt(self):
        self.m.Params.OptimalityTol = 1e-09
        self.m.Params.FeasibilityTol = 1e-09
        self.m.Params.method = 2
        self.m.Params.BarQCPConvTol = 1e-9
        self.m.Params.BarConvTol = 1e-9
        self.m.Params.QCPDual = 1
        self.m.update()
        #self.m.write('%s.lp' % self.m.model_name)
        print(self.m.modelname)
        self.m.optimize()
        if self.m.status == GRB.Status.OPTIMAL:
            print('\n OPTIMAL SOLUTION FOUND \n')
        else:
            sys.exit('\n ERROR: NO SOLUTION FOUND \n')


    def create_sets_parameters(self,Input):
    # Sets #
    ########
        if 'Players' in Input.keys():
            self.Players = Input['Players']
        if 'Time' in Input.keys():
            self.Time = Input['Time']
        if 'Scenarios' in Input.keys():
            self.Scenarios = Input['Scenarios']
    # Parameters #
    ##############
        if 'PriceBuy' in Input.keys():
            self.PriceBuy = Input['PriceBuy']
        if 'PriceSell' in Input.keys():
            self.PriceSell = Input['PriceSell']
        if 'Weight' in Input.keys():
            self.Weight = Input['Weight']
        if 'PowerPV' in Input.keys():
            self.PowerPV = Input['PowerPV']
        if 'Demand' in Input.keys():
            self.Demand = Input['Demand']
        if 'PowerPlMax' in Input.keys():
            self.PowerPlMax = Input['PowerPlMax']
        if 'PowerArbBuyMax' in Input.keys():
            self.PowerArbBuyMax = Input['PowerArbBuyMax']
        if 'PowerArbSellMax' in Input.keys():
            self.PowerArbSellMax = Input['PowerArbSellMax']
        if 'Epsilon' in Input.keys():
            self.Epsilon = Input['Epsilon']
        if 'Reg' in Input.keys():
            self.Reg = Input['Reg']

		# SP Risk aversion	
        if 'Alpha_CVaR' in Input.keys():
            self.Alpha_CVaR = Input['Alpha_CVaR']	
		
		# For AD volumes	
        if 'Opt_powerArbBuy_f' in Input.keys():
            self.Opt_powerArbBuy_f = Input['Opt_powerArbBuy_f']	
        if 'Opt_powerArbSell_f' in Input.keys():
            self.Opt_powerArbSell_f = Input['Opt_powerArbSell_f']	
        if 'Opt_powerArbBuy_s' in Input.keys():
            self.Opt_powerArbBuy_s = Input['Opt_powerArbBuy_s']	
        if 'Opt_powerArbSell_s' in Input.keys():
            self.Opt_powerArbSell_s = Input['Opt_powerArbSell_s']	
        if 'Opt_powerPl_f' in Input.keys():
            self.Opt_powerPl_f = Input['Opt_powerPl_f']	
        if 'Opt_powerPl_s' in Input.keys():
            self.Opt_powerPl_s = Input['Opt_powerPl_s']	
        if 'Opt_dual_balanceLoc_f' in Input.keys():
            self.Opt_dual_balanceLoc_f = Input['Opt_dual_balanceLoc_f']	
        if 'Opt_dual_balanceLoc_s' in Input.keys():
            self.Opt_dual_balanceLoc_s = Input['Opt_dual_balanceLoc_s']	 
        if 'Opt_pi' in Input.keys():
            self.Opt_pi = Input['Opt_pi']	
        if 'Alpha_CVaR_n' in Input.keys():
            self.Alpha_CVaR_n = Input['Alpha_CVaR_n']	
        if 'Alpha_CVaR_b' in Input.keys():
            self.Alpha_CVaR_b = Input['Alpha_CVaR_b']	

    def create_variables(self,Settings):   
    # Rn

    # First stage
        if 'powerPl_f' in Settings['variables']:
            self.powerPl_f = self.m.addVars(
            self.Players, self.Time, lb=-50, name='powerPl_f')
        if 'powerArbBuy_f' in Settings['variables']:
            self.powerArbBuy_f = self.m.addVars(
            self.Time, name='powerArbBuy_f')
        if 'powerArbSell_f' in Settings['variables']:
            self.powerArbSell_f = self.m.addVars(
            self.Time, name='powerArbSell_f')

    # Second stage
        if 'powerPl_s' in Settings['variables']:
            self.powerPl_s = self.m.addVars(
            self.Players, self.Time, self.Scenarios, lb=-50, name='powerPl_s')
        if 'powerArbBuy_s' in Settings['variables']:
            self.powerArbBuy_s = self.m.addVars(
            self.Time, self.Scenarios, name='powerArbBuy_s')
        if 'powerArbSell_s' in Settings['variables']:
            self.powerArbSell_s = self.m.addVars(
            self.Time, self.Scenarios, name='powerArbSell_s')

	# SP Risk averison
        if 'eta' in Settings['variables']:
            self.eta = self.m.addVars(
            self.Scenarios, name='eta')
        if 'var' in Settings['variables']:
            self.var = self.m.addVar(
            lb=-GRB.INFINITY, name='var')

	# AD volumes  
		# Player
        if 'eta_n' in Settings['variables']:
            self.eta_n = self.m.addVars(
            self.Players, self.Scenarios, name='eta_n')
        if 'var_n' in Settings['variables']:
            self.var_n = self.m.addVars(
            self.Players, lb=-GRB.INFINITY, name='var_n')
        if 'a_n' in Settings['variables']:
            self.a_n = self.m.addVars(
            self.Players, self.Scenarios, lb=-GRB.INFINITY, name='a_n')
		# Arbitrager
        if 'eta_b' in Settings['variables']:
            self.eta_b = self.m.addVars(
            self.Scenarios, name='eta_b')
        if 'var_b' in Settings['variables']:
            self.var_b = self.m.addVar(
            lb=-GRB.INFINITY, name='var_b')		
        if 'b' in Settings['variables']:
            self.b = self.m.addVars(
            self.Scenarios, lb=-GRB.INFINITY, name='b')


    def create_objective(self,Settings):
        if 'Rn' in Settings['objective']:
            self.Obj = self.m.setObjective(
			# first stage
            quicksum( self.PriceBuy[t]*self.powerArbBuy_f[t]
            - self.PriceSell[t]*self.powerArbSell_f[t]
			# quadratic costs for players
            + sum( (1/2)*self.powerPl_f[n,t]*self.powerPl_f[n,t]*self.Reg 
            for n in self.Players )
			# second stage
            + quicksum( self.Weight[w]*( 
            (1+self.Epsilon)*self.PriceBuy[t]*self.powerArbBuy_s[t,w]
            - (1-self.Epsilon)*self.PriceSell[t]*self.powerArbSell_s[t,w] 
			# quadratic costs for players
            + sum( (1/2)*self.powerPl_s[n,t,w]*self.powerPl_s[n,t,w]*self.Reg 
            for n in self.Players ) 
            )
            for w in self.Scenarios ) for t in self.Time )
			#
            , GRB.MINIMIZE )

        if 'Ra_sp' in Settings['objective']:
            self.Obj = self.m.setObjective(
			# first stage
            quicksum( self.PriceBuy[t]*self.powerArbBuy_f[t]
            - self.PriceSell[t]*self.powerArbSell_f[t]
			# quadratic costs for players
            + quicksum( (1/2)*self.powerPl_f[n,t]*self.powerPl_f[n,t]*self.Reg 
            for n in self.Players )
            for t in self.Time )			
			# risk premium (CVaR)
            - ( self.var - ( 1 / self.Alpha_CVaR )*quicksum(
            self.Weight[w]*self.eta[w] for w in self.Scenarios ) )
			#
            , GRB.MINIMIZE )

        if 'Ra_ADVolume' in Settings['objective']:
            self.Obj = self.m.setObjective(
			#
			# --> Player
            quicksum(
            # first stage
            quicksum( self.Opt_dual_balanceLoc_f[t]*self.Opt_powerPl_f[n,t]
			# quadratic costs for players
            + (1/2)*self.Opt_powerPl_f[n,t]*self.Opt_powerPl_f[n,t]*self.Reg
            for t in self.Time )
            # AD securities
            + quicksum( self.Opt_pi[w]*self.a_n[n,w] 
            #+ (1/2)*self.a_n[n,w]*self.a_n[n,w]*0.0001
            for w in self.Scenarios )
            # risk premium (CVaR) per player
            - ( self.var_n[n] - ( 1 / self.Alpha_CVaR_n[n] )*quicksum(
            self.Weight[w]*self.eta_n[n,w] for w in self.Scenarios ) )
            for n in self.Players )
			#
			#--> Arbitrager
            #first stage
            + quicksum(
            (self.PriceBuy[t] - self.Opt_dual_balanceLoc_f[t])*self.Opt_powerArbBuy_f[t]
            - (self.PriceSell[t] - self.Opt_dual_balanceLoc_f[t])*self.Opt_powerArbSell_f[t]
            for t in self.Time )
            # AD securities
            + quicksum( self.Opt_pi[w]*self.b[w] 
            #+ (1/2)*self.b[w]*self.b[w]*0.0001
            for w in self.Scenarios )
            # risk premium Arbitrager
            - ( self.var_b - ( 1 / self.Alpha_CVaR_b )*quicksum(
            self.Weight[w]*self.eta_b[w] for w in self.Scenarios ) )
            #
            , GRB.MINIMIZE )


    def create_constraints(self,Settings):
        # Forward market constraints for players
        ########################################
        # PowerPlMax
        if 'EQ_PowerPlMax_lb_f' in Settings['constraints']:
            self.EQ_PowerPlMax_lb_f = self.m.addConstrs(
            ( - self.PowerPlMax[n] <= self.powerPl_f[n,t]
            for n in self.Players for t in self.Time ),
            name='EQ_PowerPlMax_lb_f')
			
        if 'EQ_PowerPlMax_ub_f' in Settings['constraints']:
            self.EQ_PowerPlMax_ub_f = self.m.addConstrs(
            ( self.powerPl_f[n,t] <= self.PowerPlMax[n]
            for n in self.Players for t in self.Time ),
            name='EQ_PowerPlMax_ub_f')			


        # Spot market constraints for players
        #####################################
        # Power balance
        if 'EQ_balancePl_s' in Settings['constraints']:
            self.EQ_balancePl_s = self.m.addConstrs(
            ( self.powerPl_f[n,t] + self.PowerPV[n,t,w] 
            + self.powerPl_s[n,t,w] - self.Demand[n,t] == 0
            for n in self.Players for t in self.Time for w in self.Scenarios ),
            name='EQ_balancePl_s')

        # PowerPlMax lb
        if 'EQ_PowerPlMax_lb_s' in Settings['constraints']:
            self.EQ_PowerPlMax_lb_s = self.m.addConstrs(
            ( - self.PowerPlMax[n] <= self.powerPl_s[n,t,w]
            for n in self.Players for t in self.Time for w in self.Scenarios),
            name='EQ_PowerPlMax_lb_s')
			
        # PowerPlMax ub
        if 'EQ_PowerPlMax_ub_s' in Settings['constraints']:
            self.EQ_PowerPlMax_ub_s = self.m.addConstrs(
            ( self.powerPl_s[n,t,w] <= self.PowerPlMax[n]
            for n in self.Players for t in self.Time for w in self.Scenarios),
            name='EQ_PowerPlMax_ub_s')			


        # Forward market constraints for arbitrager
        ###########################################
        # PowerArbBuyMax
        if 'EQ_PowerArbBuyMax_f' in Settings['constraints']:
            self.EQ_PowerArbBuyMax_f = self.m.addConstrs(
            ( self.powerArbBuy_f[t] <= self.PowerArbBuyMax
            for t in self.Time ), name='EQ_PowerArbBuyMax_f')

        # PowerArbSellMax
        if 'EQ_PowerArbSellMax_f' in Settings['constraints']:
            self.EQ_PowerArbSellMax_f = self.m.addConstrs(
            ( self.powerArbSell_f[t] <= self.PowerArbSellMax
            for t in self.Time ), name='EQ_PowerArbSellMax_f')


        # Spot market constraints for arbitrager
        ########################################
        # PowerArbBuyMax
        if 'EQ_PowerArbBuyMax_s' in Settings['constraints']:
            self.EQ_PowerArbBuyMax_s = self.m.addConstrs(
            ( self.powerArbBuy_s[t,w] <= self.PowerArbBuyMax
            for t in self.Time for w in self.Scenarios),
            name='EQ_PowerArbBuyMax_s')

        # PowerArbSellMax
        if 'EQ_PowerArbSellMax_s' in Settings['constraints']:
            self.EQ_PowerArbSellMax_s = self.m.addConstrs(
            ( self.powerArbSell_s[t,w] <= self.PowerArbSellMax
            for t in self.Time for w in self.Scenarios),
            name='EQ_PowerArbSellMax_s')


        # Forward market power balance
        ##############################
        if 'EQ_balanceLoc_f' in Settings['constraints']:
            self.EQ_balanceLoc_f = self.m.addConstrs(
            ( - quicksum( self.powerPl_f[n,t] for n in self.Players )
            + self.powerArbBuy_f[t] - self.powerArbSell_f[t] == 0
            for t in self.Time ), name='EQ_balanceLoc_f')

        # Spot market power balance
        ###########################
        if 'EQ_balanceLoc_s' in Settings['constraints']:
            self.EQ_balanceLoc_s = self.m.addConstrs(
            ( - quicksum( self.powerPl_s[n,t,w] for n in self.Players )
            + self.powerArbBuy_s[t,w] - self.powerArbSell_s[t,w] == 0
            for t in self.Time for w in self.Scenarios), name='EQ_balanceLoc_s')

        # Risk aversion
        ###############
		# Ra_sp
        if 'EQ_Ra_CVaR' in Settings['constraints']:
            self.EQ_Ra_CVaR = self.m.addConstrs(
			# var
            ( self.var
			# second stage
            + quicksum( (1+self.Epsilon)*self.PriceBuy[t]*self.powerArbBuy_s[t,w]
            - (1-self.Epsilon)*self.PriceSell[t]*self.powerArbSell_s[t,w]
			# quadratic costs for players
            + sum( (1/2)*self.powerPl_s[n,t,w]*self.powerPl_s[n,t,w]*self.Reg
            for n in self.Players ) 
            for t in self.Time )
			# eta
            <= self.eta[w]
            for w in self.Scenarios) , name='EQ_Ra_CVaR')
		
		# AD Volumes        
        if 'EQ_Ra_CVaR_ad_Player' in Settings['constraints']:
            self.EQ_Ra_CVaR_ad_Player = self.m.addConstrs(
			# var
            ( self.var_n[n]
			# second stage
            + quicksum( self.Opt_dual_balanceLoc_s[t,w]*self.Opt_powerPl_s[n,t,w]  
			# quadratic costs for players
            + (1/2)*self.Opt_powerPl_s[n,t,w]*self.Opt_powerPl_s[n,t,w]*self.Reg    
            for t in self.Time )
			#AD
            - self.a_n[n,w]
			# eta
            <= self.eta_n[n,w]		
            for n in self.Players for w in self.Scenarios ), name='EQ_Ra_CVaR_ad_Player' )

        if 'EQ_Ra_CVaR_ad_Arb' in Settings['constraints']:
            self.EQ_Ra_CVaR_ad_Arb = self.m.addConstrs(
            # var
            ( self.var_b
            # second stage
            + quicksum(
            ( (1+self.Epsilon)*self.PriceBuy[t] - self.Opt_dual_balanceLoc_s[t,w]
            )*self.Opt_powerArbBuy_s[t,w]
            - ( (1-self.Epsilon)*self.PriceSell[t] - self.Opt_dual_balanceLoc_s[t,w]
            )*self.Opt_powerArbSell_s[t,w] 
            for t in self.Time )
            # AD
            - self.b[w]
            # eta
            <= self.eta_b[w]		
            for w in self.Scenarios ), name='EQ_Ra_CVaR_ad_Arb' )

        if 'EQ_Ra_Bal_ad' in Settings['constraints']:
            self.EQ_Ra_Bal_ad = self.m.addConstrs(
            ( quicksum( self.a_n[n,w] for n in self.Players ) + self.b[w] == 0
            for w in self.Scenarios ), name='EQ_Ra_Bal_ad')


    def get_results(self,Settings):
        if 'objective' in Settings['results']:
            self.objective = self.m.objVal

        ### Primal Variables ###
        if 'powerPl_f' in Settings['results']:
            self.powerPl_f = self.m.getAttr('x',self.powerPl_f)
        #
        if 'powerArbBuy_f' in Settings['results']:
            self.powerArbBuy_f = self.m.getAttr('x',self.powerArbBuy_f)     
        if 'powerArbSell_f' in Settings['results']:
            self.powerArbSell_f = self.m.getAttr('x',self.powerArbSell_f) 
        #
        if 'powerPl_s' in Settings['results']:
            self.powerPl_s = self.m.getAttr('x',self.powerPl_s)     
        #
        if 'powerArbBuy_s' in Settings['results']:
            self.powerArbBuy_s = self.m.getAttr('x',self.powerArbBuy_s)
        if 'powerArbSell_s' in Settings['results']:
            self.powerArbSell_s = self.m.getAttr('x',self.powerArbSell_s)
  
        if 'dual_balanceLoc_f' in Settings['results']:
            self.dual_balanceLoc_f = self.m.getAttr('pi',self.EQ_balanceLoc_f)
        #
        if 'dual_balanceLoc_s' in Settings['results']:
            self.dual_balanceLoc_s = {key: (1/self.Weight[key[1]])*value 
                for key,value in 
                self.m.getAttr('pi', self.EQ_balanceLoc_s).items()}

				
        ### Risk aversion ###
		# Ra_sp
        if 'eta' in Settings['results']:
            self.eta = self.m.getAttr('x',self.eta)
 
        if 'var' in Settings['results']:
            self.var = self.var.x

        if 'CVaR' in Settings['results']:
            self.CVaR = - ( self.var - ( 1/ self.Alpha_CVaR )*(
            sum(self.Weight[w]*self.eta[w] for w in self.Scenarios) ) )

        if 'dual_Ra_CVaR' in Settings['results']:
            self.dual_Ra_CVaR = self.m.getAttr('qcpi',self.EQ_Ra_CVaR)
			
		# AD Volumes
        if 'a_n' in Settings['results']:
            self.a_n = self.m.getAttr('x',self.a_n)
			
        if 'b' in Settings['results']:
            self.b = self.m.getAttr('x',self.b)			
			
        if 'dual_Ra_Bal_ad' in Settings['results']:
            self.dual_Ra_Bal_ad = self.m.getAttr('pi',self.EQ_Ra_Bal_ad)

        if 'dual_Ra_CVaR_ad_Player' in Settings['results']:
            self.dual_Ra_CVaR_ad_Player = self.m.getAttr('pi',self.EQ_Ra_CVaR_ad_Player)

        if 'dual_Ra_CVaR_ad_Arb' in Settings['results']:
            self.dual_Ra_CVaR_ad_Arb = self.m.getAttr('pi',self.EQ_Ra_CVaR_ad_Arb)
			
			
        # Payoffs from operation
		########################
        if 'OperationPayoffs' in Settings['eval']:

            # scenario specific payoff per player
            self.Payoff_per_scenario_Player = OrderedDict()
            for n in self.Players:
                for w in self.Scenarios: 
                    self.Payoff_per_scenario_Player.update(
                    {(n,w) : sum( 
                    self.dual_balanceLoc_f[t]*self.powerPl_f[n,t]
                    + (1/2)*self.powerPl_f[n,t]*self.powerPl_f[n,t]*self.Reg 
                    + self.dual_balanceLoc_s[t,w]*self.powerPl_s[n,t,w]
                    + (1/2)*self.powerPl_s[n,t,w]*self.powerPl_s[n,t,w]*self.Reg 
                    for t in self.Time ) })

            # expected payoff per player
            self.Payoff_expected_Player = OrderedDict()
            for n in self.Players:
                self.Payoff_expected_Player.update(
                {n : sum( 
                self.dual_balanceLoc_f[t]* self.powerPl_f[n,t] 
                + (1/2)*self.powerPl_f[n,t]*self.powerPl_f[n,t]*self.Reg 
                + sum( self.Weight[w]*(
                self.dual_balanceLoc_s[t,w]*self.powerPl_s[n,t,w]
                + (1/2)*self.powerPl_s[n,t,w]*self.powerPl_s[n,t,w]*self.Reg )
                for w in self.Scenarios )
                for t in self.Time ) } )

            # scenario specific payoff for arbitrager
            self.Payoff_per_scenario_Arb = OrderedDict()
            for w in self.Scenarios:
                self.Payoff_per_scenario_Arb.update(
                { w : sum( 
                ( self.PriceBuy[t] - self.dual_balanceLoc_f[t] )*(
                self.powerArbBuy_f[t] )
                - ( self.PriceSell[t] - self.dual_balanceLoc_f[t] )*(
                self.powerArbSell_f[t] )
                + ( (1+self.Epsilon)*self.PriceBuy[t] - self.dual_balanceLoc_s[t,w] )*(
                self.powerArbBuy_s[t,w] )
                - ( (1-self.Epsilon)*self.PriceSell[t] - self.dual_balanceLoc_s[t,w] )*(
                self.powerArbSell_s[t,w] ) for t in self.Time ) })

            # expected payoff for arbitrager
            self.Payoff_expected_Arb = sum( 
                ( self.PriceBuy[t] - self.dual_balanceLoc_f[t] )*(
                self.powerArbBuy_f[t] )
                - ( self.PriceSell[t] - self.dual_balanceLoc_f[t] )*(
                self.powerArbSell_f[t] )
                + sum( self.Weight[w]*(
                + ( (1+self.Epsilon)*self.PriceBuy[t] - self.dual_balanceLoc_s[t,w] )*(
                self.powerArbBuy_s[t,w] )
                - ( (1-self.Epsilon)*self.PriceSell[t] - self.dual_balanceLoc_s[t,w] )*(
                self.powerArbSell_s[t,w] ) ) for w in self.Scenarios ) 
                for t in self.Time )


        # Results from dictonary to dataframe 
		#####################################
        if 'Rn' in Settings['how']:

            # Operational results
            #####################
            self.Operation = OrderedDict()
            for w in self.Scenarios:
                self.ResultsAUX = pd.DataFrame(
                {('CoMa','dual_balanceLoc_f') : [
                self.dual_balanceLoc_f.get((t)) for t in self.Time],
                #
                ('CoMa','dual_balanceLoc_s') : [
                self.dual_balanceLoc_s.get((t,w)) for t in self.Time],
                #
                ('Arb','powerArbBuy_f') : [
                self.powerArbBuy_f.get((t)) for t in self.Time],
                #
                ('Arb','powerArbSell_f') : [
                self.powerArbSell_f.get((t)) for t in self.Time],
                #
                ('Arb','powerArbBuy_s') : [
                self.powerArbBuy_s.get((t,w)) for t in self.Time],
                #
                ('Arb','powerArbSell_s') : [
                self.powerArbSell_s.get((t,w)) for t in self.Time]
                #
                }, index=self.Time )
                for n in self.Players:
                    self.ResultsAUX[(n,'powerPl_f')] = [
                    self.powerPl_f.get((n,t)) for t in self.Time]
                    #
                    self.ResultsAUX[(n,'powerPl_s')] = [
                    self.powerPl_s.get((n,t,w)) for t in self.Time]
                    #
                self.Operation.update({ w : self.ResultsAUX })


            # Payoff per scenario
            #####################
            self.Payoff_per_scenario = pd.DataFrame({}, index=self.Scenarios)
            
            self.Payoff_per_scenario['Arb'] = ( 
                [self.Payoff_per_scenario_Arb[w] for w in self.Scenarios] )

            for n in self.Players:
                self.Payoff_per_scenario[n] = ( 
                    [self.Payoff_per_scenario_Player[n,w] for w in self.Scenarios] )

            # total payoff per scenario
            self.Payoff_per_scenario['Tot'] = (
                self.Payoff_per_scenario.sum(axis=1) )

            # Payoff expected
            #################
            self.Payoff_expected = pd.DataFrame(
                {'Arb' : [0] }, index=['Payoff'] )

            self.Payoff_expected.loc['Payoff','Arb'] = (
                self.Payoff_expected_Arb )
    
            for n in self.Players:
                self.Payoff_expected.loc['Payoff',n] = (
                    self.Payoff_expected_Player[n] )

            # total expected payoff
            self.Payoff_expected.loc['Payoff','Tot'] = (
                self.Payoff_expected.sum(axis=1)['Payoff'] )


            # scenario specific system cost
            ###############################
            self.System_Cost_per_scenario = OrderedDict()
            for w in self.Scenarios:
                self.System_Cost_per_scenario.update(
                { w : sum( 
                self.PriceBuy[t]*self.powerArbBuy_f[t]
                - self.PriceSell[t]*self.powerArbSell_f[t] 
                + sum( (1/2)*self.powerPl_f[n,t]*self.powerPl_f[n,t]*self.Reg
                for n in self.Players )
                + (1+self.Epsilon)*self.PriceBuy[t]*self.powerArbBuy_s[t,w]
                - (1-self.Epsilon)*self.PriceSell[t]*self.powerArbSell_s[t,w]  
                + sum( (1/2)*self.powerPl_s[n,t,w]*self.powerPl_s[n,t,w]*self.Reg
                for n in self.Players )
                for t in self.Time ) })

            # expected system cost
            ######################
            self.System_Cost_expected = sum(
                self.PriceBuy[t]*self.powerArbBuy_f[t]
                - self.PriceSell[t]*self.powerArbSell_f[t] 
                + sum( (1/2)*self.powerPl_f[n,t]*self.powerPl_f[n,t]*self.Reg
                for n in self.Players )
                + sum( self.Weight[w]*(
                + (1+self.Epsilon)*self.PriceBuy[t]*self.powerArbBuy_s[t,w]
                - (1-self.Epsilon)*self.PriceSell[t]*self.powerArbSell_s[t,w]  
                + sum( (1/2)*self.powerPl_s[n,t,w]*self.powerPl_s[n,t,w]*self.Reg
                for n in self.Players ) )
                for w in self.Scenarios )
                for t in self.Time )


        if 'Ra_sp' in Settings['how']:

            # Operational results
            #####################
            self.Operation = OrderedDict()
            for w in self.Scenarios:
                self.ResultsAUX = pd.DataFrame(
                {('CoMa','dual_balanceLoc_f') : [
                self.dual_balanceLoc_f.get((t)) for t in self.Time],
                #
                ('CoMa','dual_balanceLoc_s') : [
                self.dual_balanceLoc_s.get((t,w)) for t in self.Time],
                #
                ('Arb','powerArbBuy_f') : [
                self.powerArbBuy_f.get((t)) for t in self.Time],
                #
                ('Arb','powerArbSell_f') : [
                self.powerArbSell_f.get((t)) for t in self.Time],
                #
                ('Arb','powerArbBuy_s') : [
                self.powerArbBuy_s.get((t,w)) for t in self.Time],
                #
                ('Arb','powerArbSell_s') : [
                self.powerArbSell_s.get((t,w)) for t in self.Time]
                #
                }, index=self.Time )
                for n in self.Players:
                    self.ResultsAUX[(n,'powerPl_f')] = [
                    self.powerPl_f.get((n,t)) for t in self.Time]
                    #
                    self.ResultsAUX[(n,'powerPl_s')] = [
                    self.powerPl_s.get((n,t,w)) for t in self.Time]
                    #
                self.Operation.update({ w : self.ResultsAUX })


            # scenario specific system cost
            ###############################
            self.System_Cost_per_scenario = OrderedDict()
            for w in self.Scenarios:
                self.System_Cost_per_scenario.update(
                { w : sum( 
                self.PriceBuy[t]*self.powerArbBuy_f[t]
                - self.PriceSell[t]*self.powerArbSell_f[t] 
                + sum( (1/2)*self.powerPl_f[n,t]*self.powerPl_f[n,t]*self.Reg
                for n in self.Players )
                + (1+self.Epsilon)*self.PriceBuy[t]*self.powerArbBuy_s[t,w]
                - (1-self.Epsilon)*self.PriceSell[t]*self.powerArbSell_s[t,w] 
                # quadratic costs for players
                + sum( (1/2)*self.powerPl_s[n,t,w]*self.powerPl_s[n,t,w]*self.Reg 
                for n in self.Players )				
                for t in self.Time ) } )

            # risk adjusted expected system cost
            ####################################
            self.System_Cost_expected = ( sum(
                self.PriceBuy[t]*self.powerArbBuy_f[t]
                - self.PriceSell[t]*self.powerArbSell_f[t] 
                + sum( (1/2)*self.powerPl_f[n,t]*self.powerPl_f[n,t]*self.Reg
                for n in self.Players )
                for t in self.Time )
                - ( self.var - ( 1 / (self.Alpha_CVaR) )*sum(
                self.Weight[w]*self.eta[w] for w in self.Scenarios ) ) )


        if 'Ra_ADVolume' in Settings['how']:
		
            # scenario specific payoff per player
            self.Payoff_per_scenario_Player = OrderedDict()
            for n in self.Players:
                for w in self.Scenarios: 
                    self.Payoff_per_scenario_Player.update(
                    {(n,w) : sum( 
                    self.Opt_dual_balanceLoc_f[t]*self.Opt_powerPl_f[n,t]
					+ (1/2)*self.Opt_powerPl_f[n,t]*self.Opt_powerPl_f[n,t]*self.Reg
                    + self.Opt_dual_balanceLoc_s[t,w]*self.Opt_powerPl_s[n,t,w]
					+ (1/2)*self.Opt_powerPl_s[n,t,w]*self.Opt_powerPl_s[n,t,w]*self.Reg
                    for t in self.Time )
                    + sum( self.Opt_pi[w]*self.a_n[n,w] for w in self.Scenarios )
                    - self.a_n[n,w] })

            # risk adjusted expected payoff per player
            self.Payoff_expected_Player = OrderedDict()
            for n in self.Players:
                self.Payoff_expected_Player.update(
                { n : sum( 
                self.Opt_dual_balanceLoc_f[t]* self.Opt_powerPl_f[n,t] 
                + (1/2)*self.Opt_powerPl_f[n,t]*self.Opt_powerPl_f[n,t]*self.Reg
                + sum( self.Opt_pi[w]*self.a_n[n,w]	for w in self.Scenarios )			
                + sum( self.Opt_pi[w]*(
                self.Opt_dual_balanceLoc_s[t,w]*self.Opt_powerPl_s[n,t,w]
                + (1/2)*self.Opt_powerPl_s[n,t,w]*self.Opt_powerPl_s[n,t,w]*self.Reg 
                - self.a_n[n,w] )
                for w in self.Scenarios )
                for t in self.Time ) } )
				
            # scenario specific payoff for arbitrager
            self.Payoff_per_scenario_Arb = OrderedDict()
            for w in self.Scenarios:
                self.Payoff_per_scenario_Arb.update(
                { w : sum( 
                ( self.PriceBuy[t] - self.Opt_dual_balanceLoc_f[t] )*(
                self.Opt_powerArbBuy_f[t] )
                - ( self.PriceSell[t] - self.Opt_dual_balanceLoc_f[t] )*(
                self.Opt_powerArbSell_f[t] )
                + ( (1+self.Epsilon)*self.PriceBuy[t] - self.Opt_dual_balanceLoc_s[t,w] )*(
                self.Opt_powerArbBuy_s[t,w] )
                - ( (1-self.Epsilon)*self.PriceSell[t] - self.Opt_dual_balanceLoc_s[t,w] )*(
                self.Opt_powerArbSell_s[t,w] ) for t in self.Time )
                + sum( self.Opt_pi[w]*self.b[w] for w in self.Scenarios )
                - self.b[w]  })	

            # risk adjusted expected payoff for arbitrager
            self.Payoff_expected_Arb = ( sum( 
                ( self.PriceBuy[t] - self.Opt_dual_balanceLoc_f[t] )*(
                self.Opt_powerArbBuy_f[t] )
                - ( self.PriceSell[t] - self.Opt_dual_balanceLoc_f[t] )*(
                self.Opt_powerArbSell_f[t] )
                + sum( self.Opt_pi[w]*(
                + ( (1+self.Epsilon)*self.PriceBuy[t] - self.Opt_dual_balanceLoc_s[t,w] )*(
                self.Opt_powerArbBuy_s[t,w] )
                - ( (1-self.Epsilon)*self.PriceSell[t] - self.Opt_dual_balanceLoc_s[t,w] )*(
                self.Opt_powerArbSell_s[t,w] ) ) 
                for w in self.Scenarios )
                for t in self.Time )
                + sum( self.Opt_pi[w]*self.b[w] for w in self.Scenarios ) 
                - sum( self.Opt_pi[w]*self.b[w] for w in self.Scenarios ) ) 


			# PAYOFFS TO DATAFRAME #
			
            # Payoff per scenario
            #####################
            self.Payoff_per_scenario = pd.DataFrame({}, index=self.Scenarios)
            
            self.Payoff_per_scenario['Arb'] = ( 
                [self.Payoff_per_scenario_Arb[w] for w in self.Scenarios] )

            for n in self.Players:
                self.Payoff_per_scenario[n] = ( 
                    [self.Payoff_per_scenario_Player[n,w] for w in self.Scenarios] )

            # total payoff per scenario
            self.Payoff_per_scenario['Tot'] = (
                self.Payoff_per_scenario.sum(axis=1) )

            # Payoff risk adjusted expected
            ###############################
            self.Payoff_expected = pd.DataFrame(
                {'Arb' : [0] }, index=['Payoff'] )

            self.Payoff_expected.loc['Payoff','Arb'] = (
                self.Payoff_expected_Arb )
    
            for n in self.Players:
                self.Payoff_expected.loc['Payoff',n] = (
                    self.Payoff_expected_Player[n] )

            # total risk adjusted expected payoff
            self.Payoff_expected.loc['Payoff','Tot'] = (
                self.Payoff_expected.sum(axis=1)['Payoff'] )				
				
				
            # Payment flow
            ###############################
            self.PaymentFlow_expected = pd.DataFrame(
                {'Arb' : [0] }, index=['frwd_E','frwd_AD','spot_E','spot_AD','Payoff'] )

			# frwd_E
			########
            self.PaymentFlow_expected.loc['frwd_E','Arb'] = (
                sum( 
                ( self.PriceBuy[t] - self.Opt_dual_balanceLoc_f[t] )*(
                self.Opt_powerArbBuy_f[t] )
                - ( self.PriceSell[t] - self.Opt_dual_balanceLoc_f[t] )*(
                self.Opt_powerArbSell_f[t] )
                for t in self.Time ) )
    
            for n in self.Players:
                self.PaymentFlow_expected.loc['frwd_E',n] = (
                    sum( 
					self.Opt_dual_balanceLoc_f[t]* self.Opt_powerPl_f[n,t] 
					+ (1/2)*self.Opt_powerPl_f[n,t]*self.Opt_powerPl_f[n,t]*self.Reg
					for t in self.Time ) )
					
            self.PaymentFlow_expected.loc['frwd_E','Tot'] = (
                self.PaymentFlow_expected.sum(axis=1)['frwd_E'] )

			# frwd_AD
			#########
            self.PaymentFlow_expected.loc['frwd_AD','Arb'] = (
                sum( self.Opt_pi[w]*self.b[w] for w in self.Scenarios ) )
    
            for n in self.Players:
                self.PaymentFlow_expected.loc['frwd_AD',n] = (
					sum( self.Opt_pi[w]*self.a_n[n,w] for w in self.Scenarios ) )
					
            self.PaymentFlow_expected.loc['frwd_AD','Tot'] = (
                self.PaymentFlow_expected.sum(axis=1)['frwd_AD'] )
					
			# spot_E
			########
            self.PaymentFlow_expected.loc['spot_E','Arb'] = (
                sum( 
                sum( self.Opt_pi[w]*(
                + ( (1+self.Epsilon)*self.PriceBuy[t] - self.Opt_dual_balanceLoc_s[t,w] )*(
                self.Opt_powerArbBuy_s[t,w] )
                - ( (1-self.Epsilon)*self.PriceSell[t] - self.Opt_dual_balanceLoc_s[t,w] )*(
                self.Opt_powerArbSell_s[t,w] ) ) 
                for w in self.Scenarios )
                for t in self.Time ) )
    
            for n in self.Players:
                self.PaymentFlow_expected.loc['spot_E',n] = (
                    sum( 		
					sum( self.Opt_pi[w]*(
					self.Opt_dual_balanceLoc_s[t,w]*self.Opt_powerPl_s[n,t,w]
					+ (1/2)*self.Opt_powerPl_s[n,t,w]*self.Opt_powerPl_s[n,t,w]*self.Reg )
					for w in self.Scenarios )
					for t in self.Time ) )			
					
            self.PaymentFlow_expected.loc['spot_E','Tot'] = (
                self.PaymentFlow_expected.sum(axis=1)['spot_E'] )		
					
			# spot_AD
			#########
            self.PaymentFlow_expected.loc['spot_AD','Arb'] = (
                sum( - self.Opt_pi[w]*self.b[w] for w in self.Scenarios ) )
    
            for n in self.Players:
                self.PaymentFlow_expected.loc['spot_AD',n] = (
                    sum( - self.Opt_pi[w]*self.a_n[n,w] for w in self.Scenarios ) )
					
            self.PaymentFlow_expected.loc['spot_AD','Tot'] = (
                self.PaymentFlow_expected.sum(axis=1)['spot_AD'] )	
					
					
			# Total Payoff
			##############
            self.PaymentFlow_expected.loc['Payoff','Arb'] = (
                self.Payoff_expected_Arb )
    
            for n in self.Players:
                self.PaymentFlow_expected.loc['Payoff',n] = (
                    self.Payoff_expected_Player[n] )

            self.PaymentFlow_expected.loc['Payoff','Tot'] = (
                self.PaymentFlow_expected.sum(axis=1)['Payoff'] )					