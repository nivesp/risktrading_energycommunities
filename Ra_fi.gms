$ontext
Title:   Risk averse equilibrium problem of energy community
Author:  Niklas Vespermann
Date:    xx.xx.2019

Description: Model of Energy Communitiy without risk trading
$offtext

scalar starttime; starttime = jnow;

*###############################################################################
*                                  DATA
*###############################################################################
set
N                set of players
W                set of scenarios
T                set of time steps
;

Alias(w,ww)

parameter
*
PriceBuy(t)
PriceSell(t)
*
Weight(w)
PowerPV(n,t,w)
Demand(n,t)
*
Epsilon
Reg
*
PowerPlMax(n)
PowerArbBuyMax
PowerArbSellMax
*
Alpha_CVaR_n(n)
Alpha_CVaR_b
*
Z

*Load GDX Data
$gdxin E:\20_Projects\201905_RiskTrading_EnergyCommunities\20_Model\data\data_to_gams.gdx
$load n w t
$load PriceBuy, PriceSell
$load Weight, PowerPV, Demand
$load Epsilon, Reg
$load PowerPlMax, PowerArbBuyMax, PowerArbSellMax
$load Alpha_CVaR_n, Alpha_CVaR_b
$load Z
$gdxin
;


*###############################################################################
*                           Declaration of Variables
*###############################################################################
Free Variable
*
* Players
powerPl_f(n,t)
powerPl_s(n,t,w)
a_n(n,w)
*
phi_s(n,t,w)
phi_Pl_CVaR(n)
*
* Arbitrager
powerArbBuy_f(t)
powerArbSell_f(t)
powerArbBuy_s(t,w)
powerArbSell_s(t,w)
b(w)
*
phi_Arb_CVaR
*
* Price Setter
lambda_f(t)
lambda_s(t,w)
mu(w)
*
phi_Sys_CVaR
;

Positive Variable
*
* Players
chi_f_lb(n,t)
chi_f_ub(n,t)
chi_s_lb(n,t,w)
chi_s_ub(n,t,w)
*CVaR
chi_Pl_CVaR_lb(n,w)
chi_Pl_CVaR_ub(n,w)
*AD
chi_Pl_AD_lb(n,w)
chi_Pl_AD_ub(n,w)
*
prob_Pl(n,w)
*
*Arbitrager
chi_f_buy_lb(t)
chi_f_buy_ub(t)
chi_f_sell_lb(t)
chi_f_sell_ub(t)
chi_s_buy_lb(t,w)
chi_s_buy_ub(t,w)
chi_s_sell_lb(t,w)
chi_s_sell_ub(t,w)
*CVaR
chi_Arb_CVaR_lb(w)
chi_Arb_CVaR_ub(w)
*AD
chi_Arb_AD_lb(w)
chi_Arb_AD_ub(w)
*
prob_Arb(w)
*
* Price Setter
prob_Sys(w)
*
chi_Sys_CVaR_lb(w)
chi_Sys_CVaR_ub(w)
;

*-------------------------------------------------------------------------------
*                              Model
*-------------------------------------------------------------------------------
Equation
*
EQ_Pros_f_lb
EQ_Pros_f_ub
EQ_Pros_s_PB
EQ_Pros_s_lb
EQ_Pros_s_ub
EQ_Pros_CVaR
EQ_Pros_CVaR_lb
EQ_Pros_CVaR_ub
EQ_Pros_AD_lb
EQ_Pros_AD_ub
*
EQ_Arb_f_buy_lb
EQ_Arb_f_buy_ub
EQ_Arb_f_sell_lb
EQ_Arb_f_sell_ub
EQ_Arb_s_buy_lb
EQ_Arb_s_buy_ub
EQ_Arb_s_sell_lb
EQ_Arb_s_sell_ub
EQ_Arb_CVaR_b
EQ_Arb_CVaR_lb
EQ_Arb_CVaR_ub
EQ_Arb_AD_lb
EQ_Arb_AD_ub
*
EQ_CAP_CVaR
EQ_CAP_CVaR_lb
EQ_CAP_CVaR_ub
*
EQ_Pros_KKT1
EQ_Pros_KKT2
EQ_Pros_AD_KKT3
EQ_Pros_AD_KKT4
*
EQ_Arb_KKT1
EQ_Arb_KKT2
EQ_Arb_KKT3
EQ_Arb_KKT4
EQ_Arb_AD_KKT5
EQ_Arb_AD_KKT6
*
EQ_PS_KKT1
EQ_PS_KKT2
EQ_PS_AD_KKT3
*
EQ_CAP_KKT1
;

**** Prosumers Primal

EQ_Pros_f_lb(n,t).. powerPl_f(n,t)+PowerPlMax(n) =g= 0;
EQ_Pros_f_ub(n,t).. PowerPlMax(n)-powerPl_f(n,t) =g= 0;

EQ_Pros_s_lb(n,t,w).. powerPl_s(n,t,w)+PowerPlMax(n) =g= 0;
EQ_Pros_s_ub(n,t,w).. PowerPlMax(n)-powerPl_s(n,t,w) =g= 0;

EQ_Pros_s_PB(n,t,w).. powerPl_f(n,t)+powerPl_s(n,t,w)+PowerPV(n,t,w)-Demand(n,t)=e=0;

*CVaR
EQ_Pros_CVaR(n).. sum(w,prob_Pl(n,w)) =e= 1;

EQ_Pros_CVaR_lb(n,w).. prob_Pl(n,w) =g= 0;
EQ_Pros_CVaR_ub(n,w).. (1/Alpha_CVaR_n(n))*Weight(w)-prob_Pl(n,w) =g= 0;

*AD
EQ_Pros_AD_lb(n,w).. a_n(n,w)+Z(W) =g= 0;
EQ_Pros_AD_ub(n,w).. -a_n(n,w)+Z(W) =g= 0;

**** Arbitragers Primal

EQ_Arb_f_buy_lb(t).. powerArbBuy_f(t) =g= 0;
EQ_Arb_f_buy_ub(t).. PowerArbBuyMax-powerArbBuy_f(t) =g= 0;

EQ_Arb_f_sell_lb(t).. powerArbSell_f(t) =g= 0;
EQ_Arb_f_sell_ub(t).. PowerArbSellMax-powerArbSell_f(t) =g= 0;

EQ_Arb_s_buy_lb(t,w).. powerArbBuy_s(t,w) =g= 0;
EQ_Arb_s_buy_ub(t,w).. PowerArbBuyMax-powerArbBuy_s(t,w) =g= 0;

EQ_Arb_s_sell_lb(t,w).. powerArbSell_s(t,w) =g= 0;
EQ_Arb_s_sell_ub(t,w).. PowerArbSellMax-powerArbSell_s(t,w) =g= 0;

EQ_Arb_CVaR_b.. sum(w,prob_Arb(w)) =e= 1;

EQ_Arb_CVaR_lb(w).. prob_Arb(w) =g= 0;
EQ_Arb_CVaR_ub(w).. (1/Alpha_CVaR_b)*Weight(w)-prob_Arb(w) =g= 0;

*AD
EQ_Arb_AD_lb(w).. b(w)+Z(w) =g= 0;
EQ_Arb_AD_ub(w).. -b(w)+Z(w) =g= 0;


**** Price Setter Primal

EQ_CAP_CVaR.. sum(w,prob_Sys(w)) =e= 1;

EQ_CAP_CVaR_lb(w).. prob_Sys(w) =g= 0;
EQ_CAP_CVaR_ub(w).. (1/Alpha_CVaR_b)*Weight(w)-prob_Sys(w) =g= 0;


**** Prosumers KKT

EQ_Pros_KKT1(n,t).. +lambda_f(t)+powerPl_f(n,t)*Reg+sum(w,phi_s(n,t,w))-chi_f_lb(n,t)+chi_f_ub(n,t) =e= 0;

EQ_Pros_KKT2(n,t,w).. prob_Pl(n,w)*( lambda_s(t,w)+powerPl_s(n,t,w)*Reg )+phi_s(n,t,w)-chi_s_lb(n,t,w)+chi_s_ub(n,t,w) =e= 0;

* AD
*EQ_Pros_AD_KKT3(n,w).. -( sum((t),lambda_s(t,w)*powerPl_s(n,t,w)+(1/2)*powerPl_s(n,t,w)*powerPl_s(n,t,w)*Reg-a_n(n,w)) )
*                       +phi_Pl_CVaR(n)-chi_Pl_CVaR_lb(n,w)+chi_Pl_CVaR_ub(n,w) =e= 0;
EQ_Pros_AD_KKT3(n,w).. -( sum((t),lambda_s(t,w)*powerPl_s(n,t,w)+(1/2)*powerPl_s(n,t,w)*powerPl_s(n,t,w)*Reg-a_n(n,w)) )
                       -phi_Pl_CVaR(n)-chi_Pl_CVaR_lb(n,w)+chi_Pl_CVaR_ub(n,w) =e= 0;

EQ_Pros_AD_KKT4(n,w).. mu(w)-prob_Pl(n,w)-chi_Pl_AD_lb(n,w)+chi_Pl_AD_ub(n,w) =e= 0;


**** Arbitragers KKT

EQ_Arb_KKT1(t).. PriceBuy(t)-lambda_f(t)-chi_f_buy_lb(t)+chi_f_buy_ub(t) =e= 0;

EQ_Arb_KKT2(t).. -PriceSell(t)+lambda_f(t)-chi_f_sell_lb(t)+chi_f_sell_ub(t) =e= 0;

EQ_Arb_KKT3(t,w).. +prob_Arb(w)*( (1+Epsilon)*PriceBuy(t)-lambda_s(t,w) )-chi_s_buy_lb(t,w)+chi_s_buy_ub(t,w) =e= 0;

EQ_Arb_KKT4(t,w).. -prob_Arb(w)*( (1-Epsilon)*PriceSell(t)-lambda_s(t,w) )-chi_s_sell_lb(t,w)+chi_s_sell_ub(t,w) =e= 0;


* AD
*EQ_Arb_AD_KKT5(w).. -sum( t, ( (1+Epsilon)*PriceBuy(t)-lambda_s(t,w) )*powerArbBuy_s(t,w) - ( (1-Epsilon)*PriceSell(t)-lambda_s(t,w) )*powerArbSell_s(t,w) - b(w) )
*                    +phi_Arb_CVaR-chi_Arb_CVaR_lb(w)+chi_Arb_CVaR_ub(w) =e= 0;
EQ_Arb_AD_KKT5(w).. -sum( t, ( (1+Epsilon)*PriceBuy(t)-lambda_s(t,w) )*powerArbBuy_s(t,w) - ( (1-Epsilon)*PriceSell(t)-lambda_s(t,w) )*powerArbSell_s(t,w) - b(w) )
                    -phi_Arb_CVaR-chi_Arb_CVaR_lb(w)+chi_Arb_CVaR_ub(w) =e= 0;

EQ_Arb_AD_KKT6(w).. mu(w)-prob_Arb(w)-chi_Arb_AD_lb(w)+chi_Arb_AD_ub(w) =e= 0;


**** Price Setters KKT

EQ_PS_KKT1(t).. sum(n,powerPl_f(n,t))-powerArbBuy_f(t)+powerArbSell_f(t) =e= 0;

*EQ_PS_KKT2(t,w).. prob_Sys(w)*( sum(n,powerPl_s(n,t,w))-powerArbBuy_s(t,w)+powerArbSell_s(t,w) ) =e= 0;
EQ_PS_KKT2(t,w).. ( sum(n,powerPl_s(n,t,w))-powerArbBuy_s(t,w)+powerArbSell_s(t,w) ) =e= 0;

EQ_PS_AD_KKT3(w).. sum(n, a_n(n,w) )+b(w) =e= 0;

EQ_CAP_KKT1(w).. -sum(t, lambda_s(t,w)*sum(n,powerPl_s(n,t,w))-powerArbBuy_s(t,w)+powerArbSell_s(t,w) )
                 -phi_Sys_CVaR-chi_Sys_CVaR_lb(w)+chi_Sys_CVaR_ub(w) =e= 0;

*-------------------------------------------------------------------------------
*                              Model
*-------------------------------------------------------------------------------
model model_Ra_fi /
*
*Players
EQ_Pros_f_lb.chi_f_lb
EQ_Pros_f_ub.chi_f_ub
EQ_Pros_s_lb.chi_s_lb
EQ_Pros_s_ub.chi_s_ub
EQ_Pros_s_PB.phi_s
*CVaR
EQ_Pros_CVaR.phi_Pl_CVaR
EQ_Pros_CVaR_lb.chi_Pl_CVaR_lb
EQ_Pros_CVaR_ub.chi_Pl_CVaR_ub
*AD
EQ_Pros_AD_lb.chi_Pl_AD_lb
EQ_Pros_AD_ub.chi_Pl_AD_ub
*
*Arbitrageur
EQ_Arb_f_buy_lb.chi_f_buy_lb
EQ_Arb_f_buy_ub.chi_f_buy_ub
EQ_Arb_f_sell_lb.chi_f_sell_lb
EQ_Arb_f_sell_ub.chi_f_sell_ub
EQ_Arb_s_buy_lb.chi_s_buy_lb
EQ_Arb_s_buy_ub.chi_s_buy_ub
EQ_Arb_s_sell_lb.chi_s_sell_lb
EQ_Arb_s_sell_ub.chi_s_sell_ub
*CVaR
EQ_Arb_CVaR_b.phi_Arb_CVaR
EQ_Arb_CVaR_lb.chi_Arb_CVaR_lb
EQ_Arb_CVaR_ub.chi_Arb_CVaR_ub
*AD
EQ_Arb_AD_lb.chi_Arb_AD_lb
EQ_Arb_AD_ub.chi_Arb_AD_ub
*
* System Risk Agent
EQ_CAP_CVaR.phi_Sys_CVaR
EQ_CAP_CVaR_lb.chi_Sys_CVaR_lb
EQ_CAP_CVaR_ub.chi_Sys_CVaR_ub
*
*KKTs
*
*Players
EQ_Pros_KKT1.powerPl_f
EQ_Pros_KKT2.powerPl_s
EQ_Pros_AD_KKT3.prob_Pl
EQ_Pros_AD_KKT4.a_n
*
*Arbitrageur
EQ_Arb_KKT1.powerArbBuy_f
EQ_Arb_KKT2.powerArbSell_f
EQ_Arb_KKT3.powerArbBuy_s
EQ_Arb_KKT4.powerArbSell_s
EQ_Arb_AD_KKT5.prob_Arb
EQ_Arb_AD_KKT6.b
*
*Price Setter
EQ_PS_KKT1.lambda_f
EQ_PS_KKT2.lambda_s
EQ_PS_AD_KKT3.mu
*
*System risk agent
EQ_CAP_KKT1.prob_Sys
/
;

option
*limrow = 0,
*limcol = 0,
solprint = off
sysout = off;
model_Ra_fi.optfile = 1;

solve model_Ra_fi using mcp;

*--- Ex post calculations -------

Parameters
* Players
comp_Pros_f_lb
comp_Pros_f_ub
comp_Pros_PV_lb
comp_Pros_PV_ub
comp_Pros_s_lb
comp_Pros_s_ub
comp_Pros_CVaR_lb
comp_Pros_CVaR_ub
* Arbitrager
comp_Arb_f_Buy_lb
comp_Arb_f_Buy_ub
comp_Arb_f_Sell_lb
comp_Arb_f_Sell_ub
comp_Arb_s_Buy_lb
comp_Arb_s_Buy_ub
comp_Arb_s_Sell_lb
comp_Arb_s_Sell_ub
;


* Players
comp_Pros_f_lb(n,t)=[powerPl_f.l(n,t)+PowerPlMax(n)]*chi_f_lb.l(n,t);
comp_Pros_f_ub(n,t)=[PowerPlMax(n)-powerPl_f.l(n,t)]*chi_f_ub.l(n,t);
comp_Pros_s_lb(n,t,w)=[powerPl_s.l(n,t,w)+PowerPlMax(n)]*chi_s_lb.l(n,t,w);
comp_Pros_s_ub(n,t,w)=[PowerPlMax(n)-powerPl_s.l(n,t,w)]*chi_s_ub.l(n,t,w);
comp_Pros_CVaR_lb(n,w)=prob_Pl.l(n,w)*chi_Pl_CVaR_lb.l(n,w);
comp_Pros_CVaR_ub(n,w)=[(1/Alpha_CVaR_n(n))*Weight(w)-prob_Pl.l(n,w)]*chi_Pl_CVaR_ub.l(n,w);
* Arbitrager
comp_Arb_f_Buy_lb(t)=powerArbBuy_f.l(t)*chi_f_buy_lb.l(t);
comp_Arb_f_Buy_ub(t)=[PowerArbBuyMax-powerArbBuy_f.l(t)]*chi_f_buy_ub.l(t);
comp_Arb_f_Sell_lb(t)=powerArbSell_f.l(t)*chi_f_sell_lb.l(t);
comp_Arb_f_Sell_ub(t)=[PowerArbSellMax-powerArbSell_f.l(t)]*chi_f_sell_ub.l(t);
comp_Arb_s_Buy_lb(t,w)=powerArbBuy_s.l(t,w)*chi_s_buy_lb.l(t,w);
comp_Arb_s_Buy_ub(t,w)=[PowerArbBuyMax-powerArbBuy_s.l(t,w)]*chi_s_buy_ub.l(t,w);
comp_Arb_s_Sell_lb(t,w)=powerArbSell_s.l(t,w)*chi_s_sell_lb.l(t,w);
comp_Arb_s_Sell_ub(t,w)=[PowerArbSellMax-powerArbSell_s.l(t,w)]*chi_s_sell_ub.l(t,w);


Display
* Players
comp_Pros_f_lb
comp_Pros_f_ub
comp_Pros_s_lb
comp_Pros_s_ub
comp_Pros_CVaR_lb
comp_Pros_CVaR_ub
* Arbitrager
comp_Arb_f_Buy_lb
comp_Arb_f_Buy_ub
comp_Arb_f_Sell_lb
comp_Arb_f_Sell_ub
comp_Arb_s_Buy_lb
comp_Arb_s_Buy_ub
comp_Arb_s_Sell_lb
comp_Arb_s_Sell_ub
;


Parameters
system_cost_per_scenario
system_cost_expected_Player
system_cost_expected_Arb

*
Payoff_per_scenario_Player
Payoff_expected_Player
sum_prob_Player
*
Payoff_expected_Arb
Payoff_per_scenario_Arb
sum_prob_Arb
*
sum_prob_Sys
*
Payment_frwd_E_Player
Payment_frwd_AD_Player
Payment_spot_E_Player
Payment_spot_AD_Player
*
Payment_frwd_E_Arb
Payment_frwd_AD_Arb
Payment_spot_E_Arb
Payment_spot_AD_Arb
;

Alias(n,nn);

system_cost_per_scenario(w)=sum(t, PriceBuy(t)*powerArbBuy_f.l(t)-PriceSell(t)*powerArbSell_f.l(t)+sum(n, (1/2)*powerPl_f.l(n,t)*powerPl_f.l(n,t)*Reg)
                 + (1+Epsilon)*PriceBuy(t)*powerArbBuy_s.l(t,w)
                 - (1-Epsilon)*PriceSell(t)*powerArbSell_s.l(t,w)
                 + sum(n, (1/2)*powerPl_s.l(n,t,w)*powerPl_s.l(n,t,w)*Reg) ) + EPS;
system_cost_expected_Player(n)=sum(t, PriceBuy(t)*powerArbBuy_f.l(t)-PriceSell(t)*powerArbSell_f.l(t)+sum(nn, (1/2)*powerPl_f.l(nn,t)*powerPl_f.l(nn,t)*Reg)
                 + sum(w, prob_Pl.l(n,w)*(
                 + (1+Epsilon)*PriceBuy(t)*powerArbBuy_s.l(t,w)
                 - (1-Epsilon)*PriceSell(t)*powerArbSell_s.l(t,w)
                 + sum(nn, (1/2)*powerPl_s.l(nn,t,w)*powerPl_s.l(nn,t,w)*Reg) ) ) )+ EPS;
system_cost_expected_Arb=sum(t, PriceBuy(t)*powerArbBuy_f.l(t)-PriceSell(t)*powerArbSell_f.l(t)+sum(nn, (1/2)*powerPl_f.l(nn,t)*powerPl_f.l(nn,t)*Reg)
                 + sum(w, prob_Arb.l(w)*(
                 + (1+Epsilon)*PriceBuy(t)*powerArbBuy_s.l(t,w)
                 - (1-Epsilon)*PriceSell(t)*powerArbSell_s.l(t,w)
                 + sum(nn, (1/2)*powerPl_s.l(nn,t,w)*powerPl_s.l(nn,t,w)*Reg) ) ) )+ EPS;
*
Payoff_per_scenario_Player(n,w)=sum(t, lambda_f.l(t)*powerPl_f.l(n,t) + (1/2)*powerPl_f.l(n,t)*powerPl_f.l(n,t)*Reg + sum(ww,mu.l(ww)*a_n.l(n,ww))
                                 + lambda_s.l(t,w)*powerPl_s.l(n,t,w) + (1/2)*powerPl_s.l(n,t,w)*powerPl_s.l(n,t,w)*Reg - a_n.l(n,w) ) + EPS;
Payoff_expected_Player(n)=sum(t, lambda_f.l(t)*powerPl_f.l(n,t) + (1/2)*powerPl_f.l(n,t)*powerPl_f.l(n,t)*Reg + sum(w,mu.l(w)*a_n.l(n,w))
                                 + sum(w, prob_Pl.l(n,w)*[lambda_s.l(t,w)*powerPl_s.l(n,t,w) + (1/2)*powerPl_s.l(n,t,w)*powerPl_s.l(n,t,w)*Reg - a_n.l(n,w)] ) ) + EPS;
sum_prob_Player(n)=sum(w,prob_Pl.l(n,w));
*
Payoff_per_scenario_Arb(w)=sum(t, (PriceBuy(t)-lambda_f.l(t))*powerArbBuy_f.l(t)-(PriceSell(t)-lambda_f.l(t))*powerArbSell_f.l(t) + sum(ww,mu.l(ww)*b.l(ww))
                 + ((1+Epsilon)*PriceBuy(t)-lambda_s.l(t,w))*powerArbBuy_s.l(t,w)
                 - ((1-Epsilon)*PriceSell(t)-lambda_s.l(t,w))*powerArbSell_s.l(t,w) ) - b.l(w) + EPS;
Payoff_expected_Arb=sum(t, (PriceBuy(t)-lambda_f.l(t))*powerArbBuy_f.l(t)-(PriceSell(t)-lambda_f.l(t))*powerArbSell_f.l(t) + sum(w,mu.l(w)*b.l(w))
                 + sum(w, prob_Arb.l(w)*[((1+Epsilon)*PriceBuy(t)-lambda_s.l(t,w))*powerArbBuy_s.l(t,w)
                 - ((1-Epsilon)*PriceSell(t)-lambda_s.l(t,w))*powerArbSell_s.l(t,w) - b.l(w)] ) ) + EPS;
sum_prob_Arb=sum(w,prob_Arb.l(w));

*sum_prob_Sys=sum(w,prob_Sys.l(w));

Payment_frwd_E_Player(n)=   sum(t, lambda_f.l(t)*powerPl_f.l(n,t) + (1/2)*powerPl_f.l(n,t)*powerPl_f.l(n,t)*Reg )+ EPS;
Payment_frwd_AD_Player(n)=  sum(w,mu.l(w)*a_n.l(n,w) ) + EPS;
Payment_spot_E_Player(n)=   sum(t, sum(w, prob_Pl.l(n,w)*[lambda_s.l(t,w)*powerPl_s.l(n,t,w) + (1/2)*powerPl_s.l(n,t,w)*powerPl_s.l(n,t,w)*Reg ] ) ) + EPS;
Payment_spot_AD_Player(n)=  sum(w, prob_Pl.l(n,w)*[- a_n.l(n,w)] )+ EPS;
*
Payment_frwd_E_Arb=      sum(t, (PriceBuy(t)-lambda_f.l(t))*powerArbBuy_f.l(t)-(PriceSell(t)-lambda_f.l(t))*powerArbSell_f.l(t) ) + EPS;
Payment_frwd_AD_Arb=     sum(w,mu.l(w)*b.l(w)) + EPS;
Payment_spot_E_Arb=      sum(t, sum(w, prob_Arb.l(w)*[((1+Epsilon)*PriceBuy(t)-lambda_s.l(t,w))*powerArbBuy_s.l(t,w) - ((1-Epsilon)*PriceSell(t)-lambda_s.l(t,w))*powerArbSell_s.l(t,w) ] ) ) + EPS;
Payment_spot_AD_Arb=     sum(w, - prob_Arb.l(w)*[b.l(w)] ) + EPS;

Display
system_cost_per_scenario
system_cost_expected_Player
system_cost_expected_Arb
*
Payoff_per_scenario_Player
Payoff_expected_Player
sum_prob_Player
*
Payoff_per_scenario_Arb
Payoff_expected_Arb
sum_prob_Arb
*
powerArbBuy_f.l
powerArbSell_f.l
powerArbBuy_s.l
powerArbSell_s.l
prob_Arb.l
*
powerPl_f.l
powerPl_s.l
prob_Pl.l
*
lambda_f.l
lambda_s.l
*
*
phi_Pl_CVaR.l
chi_Pl_CVaR_lb.l
chi_Pl_CVaR_ub.l
*
Z
a_n.l
b.l
mu.l
*
*sum_prob_Sys
*prob_Sys.l
Payment_frwd_E_Player
Payment_frwd_AD_Player
Payment_spot_E_Player
Payment_spot_AD_Player
*
Payment_frwd_E_Arb
Payment_frwd_AD_Arb
Payment_spot_E_Arb
Payment_spot_AD_Arb
;

* Export results to .gdx file
execute_unload 'E:\20_Projects\201905_RiskTrading_EnergyCommunities\20_Model\data\gams_to_data',
system_cost_per_scenario,
system_cost_expected_Player,
system_cost_expected_Arb,
*
Payoff_per_scenario_Player,
Payoff_expected_Player,
sum_prob_Player,
*
Payoff_per_scenario_Arb,
Payoff_expected_Arb,
sum_prob_Arb,
*
powerArbBuy_f,
powerArbSell_f,
powerArbBuy_s,
powerArbSell_s,
prob_Arb,
*
powerPl_f,
powerPl_s,
prob_Pl,
*
lambda_f,
lambda_s,
*
a_n.l,
b.l,
mu.l
*
Payment_frwd_E_Player,
Payment_frwd_AD_Player,
Payment_spot_E_Player,
Payment_spot_AD_Player,
*
Payment_frwd_E_Arb,
Payment_frwd_AD_Arb,
Payment_spot_E_Arb,
Payment_spot_AD_Arb
;
