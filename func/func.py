''' 
=========================================================================
Script         : Input data
Author         : Niklas Vepermann
Project        : Risk trading in energy communities
=========================================================================
'''

# Load packages
import matplotlib.pyplot as plt
import tikzplotlib

import numpy as np

from datetime import datetime
import pandas as pd    # Pandas


def data_to_Gams(InputData):
	import pandas as pd
	import gdxpds 
	
	# Sets
	nn = pd.DataFrame( InputData['Players'] )
	nn['value'] = True
	nn = nn.rename(index=str, columns={'n':'set'})

	ww = pd.DataFrame( InputData['Scenarios'] )
	ww['value'] = True
	ww = ww.rename(index=str, columns={'w':'set'})
	
	tt = pd.DataFrame( InputData['Time'] )
	tt['value'] = True
	tt = tt.rename(index=str, columns={'t':'set'})

	# Parameters
	PriceBuy = pd.Series(InputData['PriceBuy']).rename_axis(['t']
		).reset_index(name='value')

	PriceSell = pd.Series(InputData['PriceSell']).rename_axis(['t']
		).reset_index(name='value')

	Weight = pd.Series(InputData['Weight']).rename_axis(['w']
		).reset_index(name='value')

	PowerPV = pd.Series(InputData['PowerPV']).rename_axis(['n', 't', 'w']
		).reset_index(name='value')
	
	Demand = pd.Series(InputData['Demand']).rename_axis(['n', 't']
		).reset_index(name='value')
	
	Epsilon = pd.Series(InputData['Epsilon']
		).reset_index(name='value').drop('index', axis=1)

	Reg = pd.Series(InputData['Reg']
		).reset_index(name='value').drop('index', axis=1)
		
	PowerPlMax = pd.Series(InputData['PowerPlMax']).rename_axis(['n']
		).reset_index(name='value')
	
	PowerArbBuyMax = pd.Series(InputData['PowerArbBuyMax']
		).reset_index(name='value').drop('index', axis=1)
	PowerArbSellMax = pd.Series(InputData['PowerArbSellMax']
		).reset_index(name='value').drop('index', axis=1)
	
	Alpha_CVaR_n = pd.Series(InputData['Alpha_CVaR_n']).rename_axis(['n']
		).reset_index(name='value')

	Alpha_CVaR_b = pd.Series(InputData['Alpha_CVaR_b']
		).reset_index(name='value').drop('index', axis=1)

	Z = pd.Series(InputData['Z']).rename_axis(['w']
		).reset_index(name='value')

	# assume we have a DataFrame df with last column 'value'
	data_ready_for_GAMS = { 'n': nn,
							'w': ww,
							't': tt,
							#
							'PriceBuy' : PriceBuy,
							'PriceSell' : PriceSell,
							#
							'Weight' : Weight,
							'PowerPV' : PowerPV,
							#
							'Demand' : Demand,
							#
							'Epsilon' : Epsilon,
							'Reg' : Reg,
							#
							'PowerPlMax' : PowerPlMax,
							#
							'PowerArbBuyMax' : PowerArbBuyMax,
							'PowerArbSellMax' : PowerArbSellMax,
							#
							'Alpha_CVaR_n' : Alpha_CVaR_n,
							'Alpha_CVaR_b' : Alpha_CVaR_b,
							#
							'Z' : Z
						 } 

	gdx_file = '.\data\data_to_gams.gdx'
	gdx = gdxpds.to_gdx(data_ready_for_GAMS, gdx_file)


def payoff_dist(payoff_expected,payoff_distribution,Players):
	# Players
	for n in Players:
		fig1 = plt.figure()
		medianprops = dict(linestyle=None, linewidth=0, color='firebrick')
		ax = fig1.add_subplot(111)    
		ax.boxplot(payoff_distribution[n].transpose(), whis=[5, 95], 
			showmeans=False, medianprops=medianprops)
		ax.plot( [1,2,3, 4], payoff_expected[n].transpose()[0].tolist(), 'rs' )
		plt.xticks([1, 2, 3, 4], ['fully incomp.', 'part. incomp.', 'complete', 'neutral'])
		ax.set_ylabel('Payoffs [EUR]')
		plt.title(n)
		# matplotlib2tikz.save('./data/output/graphics/%s_PayoffDist_%s.tex' 
			# % (datetime.today().strftime('%Y%m%d'), n), 
				   # figureheight = '\\fheight',
				   # figurewidth = '\\fwidth')
		plt.savefig('./data/output/graphics/%s_PayoffDist_%s.pdf' 
			% (datetime.today().strftime('%Y%m%d'), n), 
			format='pdf', dpi=1000)
	# Arbitrager
	fig2 = plt.figure()
	medianprops = dict(linestyle=None, linewidth=0, color='firebrick')
	ax = fig2.add_subplot(111)    
	ax.boxplot(payoff_distribution['Arb'].transpose(), whis=[5, 95],
		showmeans=False, medianprops=medianprops)
	ax.plot( [1,2,3, 4], payoff_expected['Arb'].transpose()[0].tolist(), 'rs' )
	plt.xticks([1, 2, 3, 4], ['fully incomp.', 'part. incomp.', 'complete', 'neutral'])
	ax.set_ylabel('Payoffs [EUR]')
	plt.title('Arbitrageur')
	# matplotlib2tikz.save('./data/output/graphics/%s_PayoffDist_Arbitrageur.tex' 
		# % (datetime.today().strftime('%Y%m%d')), 
			   # figureheight = '\\fheight',
			   # figurewidth = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_PayoffDist_Arbitrageur.pdf' 
		% (datetime.today().strftime('%Y%m%d')), 
		format='pdf', dpi=1000)
	return


def payoff_dist_tot(System_Cost_expected,System_Cost_per_scenario):
	# System Cost
	fig2 = plt.figure()
	medianprops = dict(linestyle=None, linewidth=0, color='firebrick')
	ax = fig2.add_subplot(111)    
	ax.boxplot(System_Cost_per_scenario.transpose(), whis=[5, 95],
		showmeans=False, medianprops=medianprops)
	ax.plot( [1,2,3,4], System_Cost_expected.transpose()['Player1'].tolist(), 'rs', color='green', label='Player1' )
	ax.plot( [1,2,3,4], System_Cost_expected.transpose()['Player2'].tolist(), 'rs', color='blue', label='Player2'  )
	ax.plot( [1,2,3,4], System_Cost_expected.transpose()['Player3'].tolist(), 'rs', color='yellow', label='Player3'  )
	ax.plot( [1,2,3,4], System_Cost_expected.transpose()['Arb'].tolist(), 'rs', color='red', label='Arb'  )
	plt.xticks([1, 2, 3, 4], ['fully incomp.', 'part. incomp.', 'complete', 'neutral'])
	ax.set_ylabel('Costs [EUR]')
	ax.legend(ncol=2,loc='lower left')
	plt.title('System costs')
	# matplotlib2tikz.save('./data/output/graphics/%s_SystemCostDist.tex' 
		# % (datetime.today().strftime('%Y%m%d')),
			   # figureheight = '\\fheight',
			   # figurewidth = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_SystemCostDist.pdf' 
		% (datetime.today().strftime('%Y%m%d')), 
		format='pdf', dpi=1000)																										
	return


def gams_payoff_to_df(results_GAMS,Scenarios,Players):
	# Payoff per scenario
	#####################
	Number_of_Scenarios = len(Scenarios)
	Payoff_per_scenario = pd.DataFrame({}, index=Scenarios)

	Payoff_per_scenario['Arb'] = ( 
		results_GAMS['Payoff_per_scenario_Arb']['Value'].tolist() )

	nn = 0
	for n in Players:
		Payoff_per_scenario[n] = ( 
			results_GAMS['Payoff_per_scenario_Player']['Value'][nn:nn+Number_of_Scenarios].tolist() )
		nn = nn + Number_of_Scenarios

	# total payoff per scenario
	Payoff_per_scenario['Tot'] = (
		Payoff_per_scenario.sum(axis=1) )

	# Payoff risk adjusted expected
	###############################
	Payoff_expected = pd.DataFrame(
		{'Arb' : [0] }, index=['Payoff'] )

	Payoff_expected.loc['Payoff','Arb'] = (
		results_GAMS['Payoff_expected_Arb']['Value'].tolist()[0] )

	nn = 0
	for n in Players:
		Payoff_expected.loc['Payoff',n] = (
			results_GAMS['Payoff_expected_Player']['Value'][nn:nn+1].tolist()[0] )
		nn = nn + 1

	# total risk adjusted expected payoff
	Payoff_expected.loc['Payoff','Tot'] = (
		Payoff_expected.sum(axis=1)['Payoff'] )	


	# Payment flow
	###############################
	PaymentFlow_expected = pd.DataFrame(
		{'Arb' : [0] }, index=['frwd_E','frwd_AD','spot_E','spot_AD','Payoff'] )

	# frwd_E
	########
	PaymentFlow_expected.loc['frwd_E','Arb'] = (
		results_GAMS['Payment_frwd_E_Arb']['Value'].tolist()[0] )

	nn = 0
	for n in Players:
		PaymentFlow_expected.loc['frwd_E',n] = (
			results_GAMS['Payment_frwd_E_Player']['Value'][nn:nn+1].tolist()[0] )
		nn = nn + 1
			
	PaymentFlow_expected.loc['frwd_E','Tot'] = (
		PaymentFlow_expected.sum(axis=1)['frwd_E'] )

	# frwd_AD
	#########
	PaymentFlow_expected.loc['frwd_AD','Arb'] = (
		results_GAMS['Payment_frwd_AD_Arb']['Value'].tolist()[0] )

	nn = 0
	for n in Players:
		PaymentFlow_expected.loc['frwd_AD',n] = (
			results_GAMS['Payment_frwd_AD_Player']['Value'][nn:nn+1].tolist()[0] )
		nn = nn + 1
			
	PaymentFlow_expected.loc['frwd_AD','Tot'] = (
		PaymentFlow_expected.sum(axis=1)['frwd_AD'] )
			
	# spot_E
	########
	PaymentFlow_expected.loc['spot_E','Arb'] = (
		results_GAMS['Payment_spot_E_Arb']['Value'].tolist()[0] )

	nn = 0
	for n in Players:
		PaymentFlow_expected.loc['spot_E',n] = (
			results_GAMS['Payment_spot_E_Player']['Value'][nn:nn+1].tolist()[0] )
		nn = nn + 1
			
	PaymentFlow_expected.loc['spot_E','Tot'] = (
		PaymentFlow_expected.sum(axis=1)['spot_E'] )		
			
	# spot_AD
	#########
	PaymentFlow_expected.loc['spot_AD','Arb'] = (
		results_GAMS['Payment_spot_AD_Arb']['Value'].tolist()[0] )

	nn = 0
	for n in Players:
		PaymentFlow_expected.loc['spot_AD',n] = (
			results_GAMS['Payment_spot_AD_Player']['Value'][nn:nn+1].tolist()[0] )
		nn = nn + 1
			
	PaymentFlow_expected.loc['spot_AD','Tot'] = (
		PaymentFlow_expected.sum(axis=1)['spot_AD'] )
			
			
	# Total Payoff
	##############
	PaymentFlow_expected.loc['Payoff','Arb'] = (
		results_GAMS['Payoff_expected_Arb']['Value'].tolist()[0] )

	nn = 0
	for n in Players:
		PaymentFlow_expected.loc['Payoff',n] = (
			results_GAMS['Payoff_expected_Player']['Value'][nn:nn+1].tolist()[0] )
		nn = nn + 1

	PaymentFlow_expected.loc['Payoff','Tot'] = (
		PaymentFlow_expected.sum(axis=1)['Payoff'] )


	return Payoff_per_scenario, Payoff_expected, PaymentFlow_expected
	

def plot_Ra_fi_Costs(case,rangeStudy,payoff,var_lb,var_ub):

	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	#
	ax1.plot(rangeStudy,payoff['Player1'],'-o',color='green',label='Player1')
	ax1.fill_between(rangeStudy,var_lb['Player1'], var_ub['Player1'],
		color='green',alpha=0.5,label='var')
	#
	ax1.plot(rangeStudy,payoff['Player2'],'-^',color='blue',label='Player2')
	ax1.fill_between(rangeStudy,var_lb['Player2'], var_ub['Player2'],
		color='blue',alpha=0.5,label='var')
	#
	ax1.plot(rangeStudy,payoff['Player3'],'-d',color='orange',label='Player3')
	ax1.fill_between(rangeStudy,var_lb['Player3'], var_ub['Player3'],
		color='orange',alpha=0.5,label='var')
	#
	ax1.plot(rangeStudy,payoff['Arb'],'-s',color='r',label='Arbitrageur')
	ax1.fill_between(rangeStudy,var_lb['Arb'], var_ub['Arb'],
		color='red',alpha=0.5,label='var')
	#
	ax1.set_xlabel('risk aversion alpha of Player1 [-]')
	ax1.set_ylabel('Risk adjusted expected payoffs [EUR]')
	ax1.set_xlim(max(rangeStudy),0)
	ax1.legend()
	#
	# matplotlib2tikz.save('./data/output/graphics/%s_inc_ra_%s.tex' 
		# % (datetime.today().strftime('%Y%m%d'),case),
			   # figureheight = '\\fheight',
			   # figurewidth = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_inc_ra_%s.pdf' 
		% (datetime.today().strftime('%Y%m%d'),case),
		format='pdf', dpi=1000)


def plot_bar_ADTrades(Ra_ADVolume):
	for w in Ra_ADVolume.Scenarios:
		ind = np.arange(4)
		width = 0.7
		#
		fig1 = plt.figure()
		ax = fig1.add_subplot(111)
		p1 = ax.bar(ind[0],[Ra_ADVolume.a_n['Player1',w]],width,color='black')
		p1 = ax.bar(ind[1],[Ra_ADVolume.a_n['Player2',w]],width,color='black')
		p1 = ax.bar(ind[2],[Ra_ADVolume.a_n['Player3',w]],width,color='black')
		p1 = ax.bar(ind[3],[Ra_ADVolume.b[w]],width,color='black')
		#
		ax.set_ylabel('# Arrow-Debreu securities')
		ax.set_xticks(ind)
		ax.set_xticklabels(['n1','n2','n3','Arb'])
		ax.axhline(y=0, color='black', linewidth=0.5)

		# matplotlib2tikz.save('./data/output/%s_fig_AD_Trades_%s.tex' 
			# % (datetime.today().strftime('%Y%m%d'),w), 
				   # figureheight = '\\fheight',
				   # figurewidth = '\\fwidth')
		plt.savefig('./data/output/%s_fig_AD_Trades_%s.pdf' 
			% (datetime.today().strftime('%Y%m%d'),w),
			format='pdf', dpi=1000)				


def bar_plot(InputDF,modelname):   
	N = len(InputDF.columns)
	ind = np.arange(N)    # the x locations for the groups
	width = 0.2          # the width of the bars: can also be len(x) sequence
	sep = 0.025

	fig1 = plt.figure()
	ax = fig1.add_subplot(111)
	
	# Forward
	p1 = ax.bar(ind - width - sep, InputDF.iloc[0].tolist(), width, 
		color='orange', label='Forward energy cost') # Energy forward
	p2 = ax.bar(ind - width - sep, InputDF.iloc[1].tolist(), width,
		bottom=InputDF.iloc[0].tolist(),
		color='olive',
		label='Forward Arrow-Debreu security cost') # AD forward
		
	# Spot
	p3 = ax.bar(ind, InputDF.iloc[2].tolist(), width, color='red',
		label='Spot energy cost') # Energy spot
	p4 = ax.bar(ind, InputDF.iloc[3].tolist(), width, 
		#bottom=InputDF.iloc[2].tolist(),
		color='purple',
		label='Spot Arrow-Debreu security cost') # AD spot
		
	# Balance
	p6 = ax.bar(ind + width + sep, InputDF.iloc[4].tolist(), width, color='grey', 
		label='Balance') # Balance

	# Add value of the balance
	for i, v in enumerate(InputDF.iloc[4].tolist()):
		if v >= 0:
			ax.text(i + width, 1.05*v, str(round(v,2)), color='grey', 
				ha='center', va='bottom')
		if v < 0:
			ax.text(i + width, 1.05*v, str(round(v,2)), color='grey', 
				ha='center', va='top')
	
	# Add some text for labels, title and custom x-axis tick labels, etc.
	#ax.set_ylim(min(InputDF.iloc[3].tolist()),max(InputDF.iloc[0].tolist()))
	ax.set_ylabel('Payments [EUR]')
	ax.set_xticks(ind)
	ax.set_xticklabels(list(InputDF.columns.values))
	ax.axhline(y=0, color='black', linewidth=0.5)
	ax.legend(ncol=3,loc='lower center', bbox_to_anchor=(0.5, 0.0),
		prop={'size': 6})
	plt.title(str('Payflows' + ' ' + modelname))  
	tikzplotlib.save('./data/output/graphics/%s_Payflows_%s.tex' 
		% (datetime.today().strftime('%Y%m%d'), modelname), 
			   axis_height = '\\fheight',
			   axis_width = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_Payflows_%s.pdf' 
		% (datetime.today().strftime('%Y%m%d'), modelname), 
		format='pdf', dpi=1000) 
	return			